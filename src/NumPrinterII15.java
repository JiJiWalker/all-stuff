/**
 * Created by jakub on 12.11.16.
 */
public class NumPrinterII15 {
    public static void main(String[] args) {
        int number = 0;

        while (number<10) {
            if(number%2 == 0){
                number++;
                continue;
            }else if (number%5 == 0) {
                break;
            }

            System.out.println(number+" ");
            number++;
        }


    }
}

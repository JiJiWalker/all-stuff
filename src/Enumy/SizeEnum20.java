package Enumy;

/**
 * Created by jakub on 24.12.16.
 */
public enum SizeEnum20 {
    SMALL("Mały"), MEDIUM("Średni"), LARGE("Duży");

    private String desctiption;

    SizeEnum20(String desc) {
        desctiption = desc;
    }

    public String getDesctiption() {
        return desctiption;
    }
}

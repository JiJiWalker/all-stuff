package Enumy;

/**
 * Created by jakub on 26.12.16.
 */
public enum EnumApp {
    CAT("Miau"), DOG("Hau"), MOUSE("Piii-Piii");

    private String description;

    EnumApp(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

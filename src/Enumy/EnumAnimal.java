package Enumy;

/**
 * Created by jakub on 26.12.16.
 */
public class EnumAnimal {
    public static void main(String[] args) {

        EnumApp enapp = EnumApp.CAT;

        switch (enapp) {

            case CAT:
                System.out.println("Cat");
                break;
            case DOG:
                System.out.println("Dog");
                break;
            case MOUSE:
                System.out.println("Mouse");
                break;
            default:
                System.out.println("Nothing");
                break;
        }

        EnumApp cat = EnumApp.CAT;
        System.out.println(cat.getDescription());
        System.out.println(EnumApp.CAT.name());
        System.out.println(EnumApp.CAT);

        EnumApp valof;
        valof = EnumApp.valueOf("CAT");
        System.out.println(valof);

        System.out.println(EnumApp.valueOf("DOG"));   //KlasaEnum.Zmienna == KlasaEnum.valueOf("Zmienna")

        for (EnumApp e: EnumApp.values()){
            System.out.println("Zmienne: "+e+" oraz "+e.getDescription());
        }
    }
}


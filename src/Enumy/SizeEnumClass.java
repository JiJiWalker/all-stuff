package Enumy;

/**
 * Created by jakub on 24.12.16.
 */
public class SizeEnumClass {
    public static void main(String[] args) {

        Enum2Class20 socks = new Enum2Class20();
        socks.setSize(SizeEnum20.MEDIUM);

        Enum2Class20 socks1 = new Enum2Class20();
        socks1.setSize(SizeEnum20.SMALL);

        Enum2Class20 socks2 = new Enum2Class20();
        socks2.setSize(SizeEnum20.LARGE);

        System.out.println(socks.getSize());
        System.out.println(socks.getSize().getDesctiption());
        
        System.out.println(socks1.getSize());
        System.out.println(socks1.getSize().getDesctiption());

        System.out.println(socks2.getSize());
        System.out.println(socks2.getSize().getDesctiption());


    }
}

package Lesson35;

/**
 * Created by jakub on 01.04.17.
 */
public class Task implements Comparable<Task> {
    private String name;
    private String description;
    private Priority priority;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Task(String name, String description, Priority priority) {
        this.name = name;
        this.description = description;
        this.priority = priority;
    }

    @Override
    public int compareTo(Task o) {
        return priority.compareTo(o.priority);
    }

    public enum Priority {
        HIGH, MODERATE, LOW;
    }

    @Override
    public String toString() {
        return name+" - "+description;
    }
}

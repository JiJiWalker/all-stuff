package Lesson35;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by jakub on 01.04.17.
 */
public class ClientServer {
    public static void main(String[] args) {

        Queue<Client> clientQueue = new LinkedList<>();

        clientQueue.offer(new Client("Myszka"));
        clientQueue.offer(new Client("Zośka"));
        clientQueue.offer(new Client("Robak"));
        clientQueue.offer(new Client("Wiśnia"));
        clientQueue.offer(new Client("Kurek"));

        System.out.println("Metoda peek()(1): "+clientQueue.peek());
        System.out.println("Metoda peek()(2): "+clientQueue.peek());

        System.out.println("Metoda poll()(1): "+clientQueue.poll());
        System.out.println("Metoda poll()(2): "+clientQueue.poll()+"\n");

        System.out.println("Stan kolejki: "+clientQueue);
        System.out.println("Peek "+clientQueue.peek());
        System.out.println("Stan kolejki: "+clientQueue);
        System.out.println("Poll "+clientQueue.poll());
        System.out.println("Stan kolejki: "+clientQueue);

    }
}

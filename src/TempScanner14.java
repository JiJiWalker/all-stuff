import java.util.Scanner;

/**
 * Created by jakub on 20.10.16.
 */
public class TempScanner14 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        double number = sc.nextInt();
        // po wczytaniu liczby i od razu wczytaniu słowa wczytanie słowa nie działą,.
        String word = sc.nextLine();
        // tutaj nie zapyta nas o word tylko przeskoczy od razu do sysout number.
        System.out.println(number);
        System.out.println(word);

        // sc.close();

        // Aby uniknąć powyższego przykłądu mieszania int ze string poniżej lepszy przykład.

        int number2 = sc.nextInt();
        // pobierz znak nowej linii \n
        sc.nextLine();
        // teraz możesz pobrać od użytkownika napis
        String word2 = sc.nextLine();
        // obie wartości zostają wyświetlone
        System.out.println(number2);
        System.out.println(word2);

        sc.close();
    }
}

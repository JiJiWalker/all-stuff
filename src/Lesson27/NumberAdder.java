package Lesson27;

/**
 * Created by jakub on 05.03.17.
 */
public class NumberAdder {
    public static void main(String[] args) {
        NumberAdder adder = new NumberAdder();

        int number = adder.sum(10);
    }

    public int sum(int n) {
        if (n>1) {
            System.out.println(n + "+" + "sum(" + (n-1) + ")");
            return n+sum(n-1);
        } else {
            return n;
        }
//        return n>1? n+sum(n-1) : n; // operator trójargumentowy. To samo co if coś, else coś.
    }
}

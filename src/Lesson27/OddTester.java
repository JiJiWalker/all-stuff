package Lesson27;

/**
 * Created by jakub on 07.03.17.
 */
public class OddTester {
    public static void main(String[] args) {
        OddTester od = new OddTester();
        od.checkNumber(10);
        od.checkNumber(11);

    }

    public void checkNumber(int n) {
        String check = n%2 == 0? "Parzysta" : "Nieparzysta";
        System.out.println(n+" "+check);
    }
}

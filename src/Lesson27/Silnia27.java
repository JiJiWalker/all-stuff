package Lesson27;

/**
 * Created by jakub on 07.03.17.
 */
public class Silnia27 {
    public static void main(String[] args) {
        Silnia27 zad = new Silnia27();
        int factNum = 8;
        int factRes = zad.calculateStrong(factNum);
        System.out.println("Silnia "+factNum+" = "+ factRes);

    }

    public int calculateStrong(int n) {
        if (n == 0) {
            return 1;
        } else {
            return n * calculateStrong(n-1);
        }
//        return n>1? n*calculateStrong(n-1) : 1; or this
    }

}

import java.util.Random;

/**
 * Created by jakub on 23.09.16.
 */
public class Zadanie5 {
    public static void main(String[] args) {

        Random rand = new Random();
        int x = rand.nextInt(100);
        int y = rand.nextInt(100);

        System.out.println("x = "+x);
        System.out.println("y = "+y+"\n");

        System.out.println("Czy x jest większe od y?");
        System.out.println(x>y);

        System.out.println("Czy x*2 jest większe od y?");
        System.out.println(x*2>y);

        System.out.println("Czy y < (x+3) i jednocześnie większe x-2");
        System.out.println(y<(x+3) && y>(x-2));

        System.out.println("Czy iloczyn x i y jest parzysty? (Ując modulo)");
        System.out.println((x*y)%2 == 0);

        System.out.println(x++);
        System.out.println(x);

        System.out.println(++y);

        double number1 = 10.993;
        int number2 = 5;

        //konwersja zawężająca (narrowing)
        int narrowing = (int)number1;

        //konwersja rozszerzająca (widening)
        double widening = (double)number2;

        System.out.println("Narrowing = "+narrowing);
        System.out.println("WIdening = "+widening);

    }
}

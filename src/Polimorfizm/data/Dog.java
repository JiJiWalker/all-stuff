package Polimorfizm.data;

import Polimorfizm.data.Animal;

/**
 * Created by jakub on 29.12.16.
 */
public class Dog extends Animal {

    public Dog (String name) {
        super(name);
    }

    @Override
    public void giveSound() {
        System.out.println("Jestem psem i wabie sie "+getName());
    }

    public void bark() {
        System.out.println("Woof-Woof");
    }

    public void kindness() {
        System.out.println("All i wanna do is life for you.");
    }
}

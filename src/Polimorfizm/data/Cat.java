package Polimorfizm.data;

/**
 * Created by jakub on 29.12.16.
 */
public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    @Override
    public void giveSound() {
        System.out.println("Jestem koteł i nazywam się "+getName());
    }

    public void meow() {
        System.out.println("Miauuuu");
    }

    public void kindness() {
        System.out.println("Shut up, and give me food you poor human.");
    }
}

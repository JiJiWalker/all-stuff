package Polimorfizm.data;

/**
 * Created by jakub on 29.12.16.
 */
public class Animal {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Animal(String name) {
        this.name = name;
    }

    public void giveSound() {
        System.out.println("Jestem zwierzęciem i wabie się "+getName());
    }
}

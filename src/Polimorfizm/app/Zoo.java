package Polimorfizm.app;

import Polimorfizm.data.Animal;
import Polimorfizm.data.Cat;
import Polimorfizm.data.Dog;

/**
 * Created by jakub on 29.12.16.
 */
public class Zoo {
    public static void main(String[] args) {

        /*Animal dog = new Dog("Byku");
        Animal cat = new Cat("Lili");
        Animal doge = new Animal("Kotopies");

        dog.giveSound();
        cat.giveSound();
        doge.giveSound();*/

        Animal[] animals = new Animal[3];
        animals[0] = new Dog("Byku");
        animals[1] = new Cat("Lili");
        animals[2] = new Animal("Kotopies");

        Animal dog = new Dog("Drugi Byku");
        Animal cat = new Cat("Druga Lili");

        //rzutowanie do zmiennej - PIES
        Dog catchBark = (Dog)dog;
        catchBark.bark();

        Dog catchKind = (Dog)dog;
        catchKind.kindness();

        //rzutowanie "w locie" - KOT
        ((Cat)cat).meow();
        ((Cat) cat).kindness();

        //rzutowanie do zmiennej - KOT
        Cat catKind = (Cat)cat;
        catKind.kindness();

        changeAnimalName(animals[1], "Nie ma kota");

        for (Animal a: animals){
            a.giveSound();
        }

    }

    public static void changeAnimalName(Animal animal, String newName) {
        animal.setName(newName);
    }

}

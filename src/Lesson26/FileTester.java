package Lesson26;

import java.io.*;

/**
 * Created by jakub on 01.03.17.
 */
public class FileTester {
    public static void main(String[] args) {
        String fileName = "testFile.txt";
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileName));
            String tmp;
            while ((tmp = reader.readLine()) != null) {
                System.out.println(tmp);
            }
        } catch ( IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write("Pierwsza linia");
            writer.newLine();
            writer.write("Druga linia");
            writer.newLine();
            writer.write(700);
        } catch (IOException e) {
            System.err.println("Błąd zapisu do pliku.");
        }
    }
}

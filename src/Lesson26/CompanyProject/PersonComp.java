package Lesson26.CompanyProject;

import java.io.Serializable;

/**
 * Created by jakub on 02.03.17.
 */
public class PersonComp implements Serializable {
    public static final long serialVersionUID = 1L;

    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public PersonComp() {}

    public PersonComp(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return " " + firstName +
                 " " + lastName;
    }
}

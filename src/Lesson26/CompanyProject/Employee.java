package Lesson26.CompanyProject;

/**
 * Created by jakub on 02.03.17.
 */
public class Employee extends PersonComp {
    public static final long serialVersionUID = 2L;

    private double salary;

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Employee() {}

    public Employee(String firstName, String lastName, double salary) {
        super(firstName, lastName);
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee:" + super.toString()+ ", " + salary + " zŁ.";
    }
}

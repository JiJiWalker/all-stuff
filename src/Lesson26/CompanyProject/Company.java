package Lesson26.CompanyProject;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by jakub on 02.03.17.
 */
public class Company implements Serializable {
    public static final long serialVersionUID = 3L;
    public static final int EMPLOYEES = 2;
    private Employee[] employees;

    public Employee[] getEmployees() {
        return employees;
    }

    public void setEmployees(Employee[] employees) {
        this.employees = employees;
    }

    public Company() {
        employees = new Employee[EMPLOYEES];
    }

    public void add(Employee emp, int index) {
        employees[index] = emp;
    }

}

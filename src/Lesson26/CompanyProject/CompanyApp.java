package Lesson26.CompanyProject;

import java.io.*;
import java.util.Scanner;

/**
 * Created by jakub on 02.03.17.
 */
public class CompanyApp {
    private static final String FILE_STORE = "employees.info";
    private static final int READ_FROM_USER = 1;
    private static final int READ_FROM_FILE = 2;

    public static void main(String[] args) {
        CompanyApp oc = new CompanyApp();
        Company company = new Company();
//        Employee emp = new Employee("Patryk", "Kossowski", 2000);

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter employee data - "+READ_FROM_USER);
        System.out.println("Read employee data - "+READ_FROM_FILE);

        int option = sc.nextInt();

        if (option == READ_FROM_USER) {
            oc.writeData(company);
        } else if (option == READ_FROM_FILE) {
            oc.readData();
        }
        sc.close();
    }

    private void writeData(Company company) {
        try (Scanner sc = new Scanner(System.in);
             FileOutputStream fos = new FileOutputStream(FILE_STORE);
             ObjectOutputStream oos = new ObjectOutputStream(fos)
            ) {
                for (int i = 0; i < Company.EMPLOYEES; i++) {
                    System.out.println("Enter fisrt name: ");
                    String fn = sc.nextLine();
                    System.out.println("Enter last name");
                    String ls = sc.nextLine();
                    System.out.println("Enter salary: ");
                    double salary = sc.nextDouble();
                    sc.nextLine();
                    System.out.println("Next one?");

                    company.add(new Employee(fn, ls, salary), i);
                }

                oos.writeObject(company);

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("File saved. ");
    }

    private void readData() {
        try (FileInputStream fis = new FileInputStream(FILE_STORE);
            ObjectInputStream ois = new ObjectInputStream(fis)
            ){

            Company company = (Company) ois.readObject();

            for (int i=0; i<Company.EMPLOYEES; i++) {
                System.out.println(company.getEmployees()[i]);
            }


        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

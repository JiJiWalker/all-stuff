package Lesson26;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Created by jakub on 01.03.17.
 */
public class ObjectReader {
    public static void main(String[] args) {
        String fileName = "person.obj";

        Person p1 = null;

        try (FileInputStream fis = new FileInputStream(fileName);
             ObjectInputStream ois = new ObjectInputStream(fis);
             ) {

            p1 = (Person) ois.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("IOException error");
        } catch (ClassNotFoundException e) {
            System.err.println("ClassNotFoundException error");
        }

        if (p1 != null) {
            System.out.println("Wczytano dane o: ");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(p1.getFirstName() + " " + p1.getLastName());
        }
    }
}

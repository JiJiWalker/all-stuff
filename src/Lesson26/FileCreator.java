package Lesson26;

import java.io.File;
import java.io.IOException;

/**
 * Created by jakub on 28.02.17.
 */
public class FileCreator {
    public static void main(String[] args) {
        String fileName = "testFile.txt";
        File file = new File(fileName);

        boolean fileExist = file.exists();
        if (!fileExist) {
            try {
                fileExist = file.createNewFile();
            } catch (IOException e) {
                System.out.println("Nie udało się utworzyć pliku");
            }
        }

        if (fileExist)
            System.out.println("Plik "+fileName+" istnieje lub został utworzony.");
    }
}

package Lesson26;

import java.io.Serializable;

/**
 * Created by jakub on 01.03.17.
 */
public class Person implements Serializable {
    public static final long serialVersionUID = 381201717708822612L;

    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}

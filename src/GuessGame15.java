import java.util.Random;
import java.util.Scanner;

/**
 * Created by jakub on 03.11.16.
 */
public class GuessGame15 {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Random rand = new Random();
        int value = rand.nextInt(501);


        int number = 500;
        int userInput;
        System.out.println("Guess the number!");

        while ((userInput = reader.nextInt()) !=number) {
            if (userInput<=450 && userInput>=0) {
                System.out.println("Your number is to low. Try Again");
            } else if (userInput>=451 && userInput<= 480){
                System.out.println("Your number is pretty close. Try harder");
            } else if (userInput>=481 && userInput<=499) {
                System.out.println("Almost there!");
            } else if (userInput>=501 && userInput<= 520){
                System.out.println("Almost (lower) there!");
            } else if (userInput>=521 && userInput<= 550) {
                System.out.println("Number is pretty close. Try lower");
            } else if (userInput>=551) {
                System.out.println("Numbes too high!");
            }
        }

        System.out.println("Congratulations. You wasted your time for this pointless number.");

        reader.close();
    }
}

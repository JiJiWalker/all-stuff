package Projekcje.data;

import javax.xml.crypto.Data;

/**
 * Created by jakub on 09.10.16.
 */
public class DataGame {

    String title;
    int edge;
    String gatunek;
    String platform;
    int amount;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getEdge() {
        return edge;
    }

    public void setEdge(int edge) {
        this.edge = edge;
    }

    public String getGatunek() {
        return gatunek;
    }

    public void setGatunek(String gatunek) {
        this.gatunek = gatunek;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public DataGame() {

    }

    public DataGame(String title) {
        this.title = title;
    }

    public DataGame(String title, String gatunek, String platform, int edge, int amount ) {
        this.title = title;
        this.gatunek = gatunek;
        this.platform = platform;
        this.edge = edge;
        this.amount = amount;
    }

    public void printInfo() {
        String info = "Tytuł: "+getTitle()+
                ". Gatunek: "+getGatunek()+
                ". Platforma: "+getPlatform()+
                ". PEGI: "+getEdge()+
                "+. Ilość: "+getAmount()+".";
        System.out.println(info);
    }

//    dodaje 1 do ilości posiadanych
    public void addAmountOne() {
        amount++;
    }

//    odejmuje '1' od amount
    public void minusAmountOne() {
        amount--;
    }

//    metoda która dodaje 'x' do amount
    public void addX(int x) {
        setAmount(getAmount()+x);
    }

//    metoda która odejmuje 'x' od amount
    public void minusX(int x) {
        setAmount(getAmount()-x);
    }
}

package Projekcje.data;

import java.util.Date;

/**
 * Created by jakub on 07.10.16.
 */
public class DaneProjekcji {

    private String name;
    private int age;
    private int lenght;
    private int lvl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLenght() {
        return lenght;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public DaneProjekcji() {

    }

    public DaneProjekcji(String name, int lvl) {
        this.name = name;
        this.lvl = lvl;
    }

    public DaneProjekcji(String name, int age, int lenght, int lvl) {
        this.name = name;
        this.age = age;
        this.lenght = lenght;
        this.lvl = lvl;
    }

    public void printInfo() {
        String info = "Imię: "+getName()+"\n"+
                        "Wiek: "+getAge()+"\n"+
                        "Długość: "+getLenght()+" min."+"\n"+
                        "Poziom: "+getLvl()+"\n";
        System.out.println(info);
    }
}

package Projekcje.app;

import Projekcje.data.DataGame;

/**
 * Created by jakub on 09.10.16.
 */
public class ShopGame {
    public static void main(String[] args) {

        DataGame gra = new DataGame("Fifa 16", "Sport", "PC/PS4", 12, 10);

        gra.printInfo();

        gra.addAmountOne();
        gra.printInfo();

        gra.addX(5);
        gra.printInfo();

        gra.minusX(16);
        gra.printInfo();

    }
}

import java.util.Scanner;

/**
 * Created by jakub on 31.10.16.
 */
public class NumberChecker15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int tmp;
        for (int i=0; i<3; i++) {
            System.out.println("Podaj liczbę do sprawdzenia: ");
            tmp = sc.nextInt();

            if (tmp % 2 == 0) {
                System.out.println("Twoja liczba"+" ("+tmp+") jest parzysta.");
            } else {
                System.out.println("Twoja liczba"+" ("+tmp+") nie jest parzysta");
            }
        }

        sc.close();

    }
}

import java.util.Scanner;

/**
 * Created by jakub on 20.10.16.
 */
public class InOut14 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String firstName;
        String lastName;
        byte age;

        System.out.println("Wprowadź swoje imię.");
        firstName = input.nextLine();
        System.out.println("Wprowadź nazwisko.");
        lastName = input.nextLine();
        System.out.println("Podaj swój wiek.");
        age = input.nextByte();

        input.close();

        System.out.println("Cześć "+firstName+" "+lastName);
        System.out.println("Masz "+age+" lat.");
    }
}

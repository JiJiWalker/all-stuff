/**
 * Created by jakub on 30.09.16.
 */
public class Zadanie8Calculator {

    int add(int a, int b) {
        return a+b;
    }

    int add(int a, int b, int c) {
        return add(a, b)-c;
    }

    double add(double a, double b) {
        return a+b;
    }

    int substract(int a, int b, int c) {
        return a-b-c;
    }

    int substract(int a, int b) {
        return b-a;
    }

    double substract(double a, double b, double c) {
        return b-c*a;
    }

}

package BookProject.app;

import java.util.NoSuchElementException;

/**
 * Created by jakub on 29.12.16.
 */
public enum Option {
    EXIT(0, " - wyjście z programu."),
    ADD_BOOK(1, " - dodanie nowej książki."),
    ADD_MAGAZINE(2, " - dodanie nowego magazynu."),
    PRINT_BOOKS(3, " - wyświetl dostępne książki."),
    PRINT_MAGAZINES(4, " - wyświetl dostępne magazyny.");

    private int value;
    private String description;

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    Option(int value, String description) {
        this.value = value;
        this.description = description;
    }

    @Override
    public String toString() {
        return value + description;
    }

    public static Option createFromInt(int option) throws NoSuchElementException {
        Option result = null;
        try {
            result = Option.values()[option];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new NoSuchElementException("Brak elementu o wskazanym ID");
        }
        return Option.values()[option];
    }
}

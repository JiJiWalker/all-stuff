package BookProject.app;

import BookProject.data.Library;
import BookProject.data.Publication;

/**
 * Created by jakub on 03.12.16.
 */
public class LibraryApp {
    public static void main(String[] args) {
        final String appName = "Biblioteka v0.92";
        System.out.println(appName);
        LibraryControl libCon = new LibraryControl();
        libCon.controlLoop();


    }
}

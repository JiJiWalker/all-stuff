package BookProject.data;

/**
 * Created by jakub on 27.09.16.
 */
public class Book extends Publication{
    private static final long serialVersionUID = 2L;

    private String author;
    private int pages;
    private String isbn;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    // Zmiana 0.3. Nowy konstruktor.
    public Book(String title, String author, int year, int pages, String publisher, String isbn) {
        super(year, title, publisher);
        this.setAuthor(author);
        this.setPages(pages);
        this.setIsbn(isbn);
    }

    @Override
    public String toString() {
        StringBuilder print = new StringBuilder(32);
        print.append(getTitle());
        print.append("; ");
        print.append(getAuthor());
        print.append("; ");
        print.append(getYear());
        print.append("; ");
        print.append(getPages());
        print.append("; ");
        print.append(getPublisher());
        print.append("; ");
        print.append(getIsbn());
        print.append("; ");
        print.append("; ");
        return print.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        if (getPages() != book.getPages()) return false;
        if (!getAuthor().equals(book.getAuthor())) return false;
        return getIsbn().equals(book.getIsbn());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getAuthor().hashCode();
        result = 31 * result + getPages();
        result = 31 * result + getIsbn().hashCode();
        return result;
    }

    public Book(Book book) {
        this(book.getTitle(), book.getAuthor(), book.getYear(), book.getPages(), book.getPublisher(), book.getIsbn());
    }
    

    // Zmiana 0.3. Metoda nic nie zwracająca. Ułatwia wywoływanie.
    /*public void printInfo() {
        String info ="Tytuł: "+getTitle()+"\n"
                    +"Autor: "+getAuthor()+"\n"
                    +"Data wydania: "+ getYear()+"\n"
                    +"Ilość stron: "+getPages()+"\n"
                    +"Wydawnictwo: "+getPublisher()+"\n"
                    +"ISBN: "+getIsbn()+"\n";
        System.out.println(info);
    }*/ // Zmiana nastąpiłą po dodaniu toString

}

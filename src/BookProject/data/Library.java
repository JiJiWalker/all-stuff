package BookProject.data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 20.09.16.
 */
public class Library implements Serializable {
    private static final long serialVersionUID = 4L;

    /**public static void main(String[] args) {      Zmienione w likcji 15.

        final String appName = "Library v 0.6";

        Pierwsza wersja 0.1. Wszystko ręcznie, bez konstruktorów.
        BookProject.data.Book book1 = new BookProject.data.Book();
        book1.title = "W pustyni i w puszczy";
        book1.author = "Henryk Sienkiewicz";
        book1.releaseDate = 2010;
        book1.pages = 296;
        book1.publisher = "Greg";
        book1.isbn = "9788373271890";*/

        /**System.out.println(appName+"\n");
        System.out.println("Książki dostępne w bibliotece:");
        System.out.println(book1.title);
        System.out.println(book1.author);
        System.out.println(book1.releaseDate);
        System.out.println(book1.pages);
        System.out.println(book1.publisher);
        System.out.println(book1.isbn);

        Book[] books = new Book[1000];
        DataReader dataReader = new DataReader();

        //books[0] = new Book("W pustyni i w puszczy", "Henryk Sienkiewicz", 2010, 296, "Greg", "9788373271890");
        //books[1] = new Book("Java. Efektywne programowanie. Wydanie II", "Joshua Bloch", 2009, 352, "Helion", "9788324620845");
        books[2] = new Book("SCJP Sun Certified Programmer for java 6 Study Guide", "Bert Bates, Katherine Sierra", 2008, 851, "McGraw-Hill Osborne Media", "9780071591065");

        System.out.println("\t"+appName+"\n");
        System.out.println("Wprowadź nową książkę.");
        books[0] = dataReader.readAndCreateBook();
        books[1] = dataReader.readAndCreateBook();
        dataReader.close();
        System.out.println("Książki dostępne w bibliotece: "+"\n");

        books[0].printInfo();
        books[1].printInfo();
        books[2].printInfo();
        System.out.println("System może przechowywać do "+books.length+" książek.");
    }*/

//    public static final int INITIAL_CAPACITY = 1;
    //Zmieniony typ.
    private Map<String, Publication> publications;
    //Dodane.
    private Map<String, LibraryUser> users;
//    private int publicationNumber;

    //Zwracamy rozmiar mapy.
    public int getPublicationsNumber() {
        return publications.size();
    }

    //Zmieniony typ
    public Map<String, Publication> getPublications() {
        return publications;
    }

    //Dodany getter
    public Map<String, LibraryUser> getUsers() {
        return users;
    }

    public Library() {
        //Zmieniony typ
        publications = new HashMap<>();
        //Dodane
        users = new HashMap<>();
    }

    public void addBook(Book book) {
        addPublication(book);
    }

    public void addMagazine(Magazine magazine) {
        addPublication(magazine);
    }

    //Dodane
    public void addUser(LibraryUser user) {
        users.put(user.getPesel(), user);
    }

    //Zmieninona Logika
    public void removePublication(Publication pub) {
        if (publications.containsValue(pub)) {
            publications.remove(pub.getTitle());
        }
    }

    //Zmieniona logika i usunięcie zwracanego typu
    public void addPublication(Publication pub) {
        publications.put(pub.getTitle(), pub);
    }

    //Zmieniona logika pętli
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Publication p: publications.values()) {
            builder.append(p);
            builder.append("\n");
        }
        return builder.toString();
    }

//    public void printBooks() {
//        int countBooks = 0;
//        for (int i = 0; i < publicationNumber; i++) {
//            if (publications[i] instanceof Book) {
//                System.out.println(publications[i]);
//                countBooks++;
//            }
//        }
//        if (countBooks == 0) {
//            System.out.println("Brak książek w bibliotece.");
//        }
//    }
//
//    public void printMagazines() {
//        int countMagazines = 0;
//        for (int i = 0; i < publicationNumber; i++) {
//            if (publications[i] instanceof Magazine) {
//                System.out.println(publications[i]);
//                countMagazines++;
//            }
//            if (countMagazines == 0) {
//                System.out.println("Brak magazynów w bibliotece.");
//            }
//
//        }
//    }

//    poniższe Comparatory zostały dodane wg prowadzenia lekcji. Nie widać żeby były klasami anonimowyim,
//    ale mimo wszystko są klasami w klasie. Nie praktukuje tego normalnie, ale nie chce mi się dodatkowo zmieniać
//    kodu aby to działało. Po lekturze są to statyczne klasy zagnieżdzone. Dzieki temu nie musimy tworzyć instancji
//    klasy Library do posługiwania się nimi.

    public static class AlphabeticalComparator implements Comparator<Publication> {
        @Override
        public int compare(Publication o1, Publication o2) {
            if (o1 == null && o2 == null) {
                return 0;
            }
            if (o1 == null) {
                return 1;
            }
            if (o2 == null) {
                return -1;
            }
            return o1.getTitle().compareTo(o2.getTitle());
        }
    }

    public static class DateComparator implements Comparator<Publication> {
        @Override
        public int compare(Publication o1, Publication o2) {
            if (o1 == null && o2 == null) {
                return 0;
            }
            if (o1 == null) {
                return 1;
            }
            if (o2 == null) {
                return -1;
            }
            Integer i1 = o1.getYear();
            Integer i2 = o2.getYear();
            return -i1.compareTo(i2);
        }
    }
}

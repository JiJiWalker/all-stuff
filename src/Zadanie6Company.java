/**
 * Created by jakub on 27.09.16.
 */
public class Zadanie6Company {
    public static void main(String[] args) {

        Zadanie6Employee p1 = new Zadanie6Employee();
        p1.firstName = "Marek";
        p1.subName = "Markowicz";
        p1.birthday = 1990;
        p1.experience = 3;

        System.out.println("Imię: "+p1.firstName+"\n"
                +"Nazwisko: "+p1.subName+"\n"
                +"Rok urodzenia: "+p1.birthday+"\n"
                +"Doświadczenie: "+p1.experience+" lat."+"\n");

        Zadanie6Employee p2 = new Zadanie6Employee();
        p2.firstName = "Arek";
        p2.subName = "Arkowicz";
        p2.birthday = 1992;
        p2.experience = 5;

        System.out.println("Imię: "+p2.firstName+"\n"
                +"Nazwisko: "+p2.subName+"\n"
                +"Rok urodzenia: "+p2.birthday+"\n"
                +"Doświadczenie: "+p2.experience+" lat."+"\n");

        Zadanie6Employee p3 = new Zadanie6Employee();
        p3.firstName = "Darek";
        p3.subName = "Darkowicz";
        p3.birthday = 1996;
        p3.experience = 1;

        System.out.println("Imię: "+p3.firstName+"\n"
                +"Nazwisko: "+p3.subName+"\n"
                +"Rok urodzenia: "+p3.birthday+"\n"
                +"Doświadczenie: "+p3.experience+" lat."+"\n");


    }
}

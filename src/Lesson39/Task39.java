package Lesson39;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Jakub on 01.05.2017.
 */
public class Task39 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Hania", "Ania", "Zuzia", "Bartek", "Roman");

        names.sort(String::compareToIgnoreCase);

        for (String s: names) {
            System.out.println(s);
        }
    }
}

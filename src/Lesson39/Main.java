package Lesson39;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Consumer;

/**
 * Created by Jakub on 08.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        String[] names = {"Kwalski", "Nowak", "Rębacki", "Duchowicz", "Malkowicz"};
        Arrays.sort(names, Main::sortAscending);
        consumerArray(names, System.out::println);

        System.out.println(">>>");
        Arrays.sort(names, Main::sortDescending);
        consumerArray(names, System.out::println);
    }

    private static <T> void consumerArray(T[] arr, Consumer<T> consumer) {
        for (T t: arr) {
            consumer.accept(t);
        }
    }

    private static int sortAscending(String t1, String t2) {
        return t1.compareTo(t2);
    }

    private static int sortDescending(String t1, String t2) {
        return t2.compareTo(t1);
    }
}

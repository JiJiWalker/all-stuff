package Other;

/**
 * Created by jakub on 06.09.16.
 */
class Frog {
    String name;
    int age;

    public String getName() {
        return name;
    }

    public void setName(String name) { //setter.
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
public class SettersThis {

    public static void main(String[] args){

        Frog frog1 = new Frog();

        frog1.setName("Joanna");
        frog1.setAge(2);

    }
}

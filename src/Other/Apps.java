package Other;

/**
 * Created by jakub on 14.09.16.
 */
class Frogg {

    String name;
    int age;

    public void setName(String newName) {
        this.name = newName;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}

public class Apps {

    public static void main(String[] args) {

        Frogg frogg1 = new Frogg();
        // frogg1.name = "Zaba";
        // frogg1.age = 2;

        frogg1.setName("Zabka");

        System.out.println(frogg1.getName());

    }
}

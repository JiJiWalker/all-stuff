package Other;

public class Rozwiazanie {
	public static void main(String[] args){
		short a = 5;
		final int b = 129;
		final char c = 'z';
		
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		
		String x = "Ala";
		String y = "ma";
		String z = "kota";
		String zdanie = x+y+z;
		
		System.out.println(zdanie);
		System.out.println(zdanie.substring(0, 7));
	}
}

package Other;

class Robo {

    String name;
    byte age;
    double wzrost;
    String gender;

     public Robo() {

     }

     public Robo(String name, byte age) {
         this.name = name;
         this.age = age;
     }


}


public class ClassesObjects {


    public void Koles() {

        Robo r1 = new Robo();
        r1.name = "Jackob";
        r1.age = (byte) 20;
        r1.wzrost = 1.85;
        r1.gender = "Male";

        Robo r2 = new Robo();
        r2.name = "Ania";
        r2.age = (byte) 30;
        r2.wzrost = 1.50;
        r2.gender = "Female";

        System.out.println("My name is "+r1.name+" and I'm "+r1.age+" years old.");


    }


}


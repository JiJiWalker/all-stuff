package Other;

import java.util.Scanner;
public class Adventure
{
    public static void main(String[] args)
    {
        Scanner myScan = new Scanner (System.in);
        System.out.println("What's your name?");
        String name = myScan.nextLine();

        while (!(name == ""))   //Always returns false.
        {
            System.out.println("That's not your name. Please try again.");
            name = myScan.nextLine();
        }

        System.out.println("It's a pleasure to meet you, " + name + ".");
    }
}
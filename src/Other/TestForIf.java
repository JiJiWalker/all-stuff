package Other; /**
 * Created by jakub on 16.06.2016.
 */
import java.util.Scanner;

public class TestForIf {
    public static void main(String[] args) {
        int a = 2;
        int b = 3;
        int c = 5;
        int d;
        int e;
        double a1;
        double a2;
        double a3;

        Scanner odczyt = new Scanner(System.in);

        System.out.println("Próba pierwsza 'while'.\nPodaj imię: ");
        String aa = odczyt.nextLine();
        String dd;



        while (!aa.equals("Jakub") && !aa.equals("jakub")) { // nie było wykrzyknika przy Ani. Nie działała cała pętla.
            if (aa.equals("Nie") || aa.equals("nie")) {
                System.out.println("Wybrałeś nie. Podaj imię.");
                aa = odczyt.nextLine();
            }else if (aa.equals("Ania") || aa.equals("Aneczka")) {

                System.out.println("Cześć Aniu. Powiedz ładnie do Kubusia.");
                aa = odczyt.nextLine();
                while (!aa.equals("Myniu") && !aa.equals("Myszko")) {
                    if (aa.equals("Jakub") || aa.equals("Kuba")) {
                        System.out.println("Wybrałaś imię. Potrzebne będzie później. Teraz troszkę ładniej.");
                        aa = odczyt.nextLine();
                    } else if (aa.equals("Kocham Cię") || aa.equals("kocham Cię")) {
                        System.out.println("Wybrałaś miłość. Ja Ciebie też. Próbuj dalej");
                        aa = odczyt.nextLine();
                    } else {
                        System.out.println("Napisałaś łądnie, ale hasło jest inne. Próbuj jednak dalej.");
                        aa = odczyt.nextLine();
                    }
                }
            }else if (aa.equals("Myniu") || aa.equals("Myszko")){
                System.out.println("Brawo. Podaj teraz moje prawdziwę imię.");
                aa = odczyt.nextLine();
            } else {
                System.out.println("Wybrałeś coś czego nie znam "+aa+". Podaj imię.");
                aa = odczyt.nextLine();
            }
        }


        /*while (!aa.equals("Jakub") && !aa.equals("jakub")){
            System.out.println("Nie Ciebie szukam "+aa+". Próbuj dalej"); // Stara pętla "while".
            aa = odczyt.nextLine();
        }*/

        /*System.out.println("Witam Aniu. Podaj hasło:");
        String dd;
        dd = odczyt.nextLine();

        while (!dd.equals("Myniu") && !dd.equals("Mynio") && !dd.equals("Myszka") && !dd.equals("Myszko") && !dd.equals("Kubunciu") && !dd.equals("")){
            if (dd.equals("Kubuś") || dd.equals("Jakub") || dd.equals("Jakubuś") || dd.equals("")){
                System.out.println("Blisko. Postaraj się bardziej :).");
                dd = odczyt.nextLine();
            }else if (dd.equals("Kocham Cię") || dd.equals("Kocham Cie") || dd.equals("Kocham Cię.")){
                System.out.println("Ja Ciebie też, Sikoreczko. Próbuj dalej");
                dd = odczyt.nextLine();
            }else{
                System.out.println("Wybrałaś coś czego nie przewidziałem. Próbuj dalej");
                dd = odczyt.nextLine();
            }
        }*/

        System.out.println("Witam "+aa);

        System.out.println("Próba druga 'do/while'.\nPodaj imię: ");
        String bb = odczyt.nextLine();


        do{
            System.out.println("Teraz działa 'do'. Pywnyś "+bb+" ?");
            bb = odczyt.nextLine();

                /*if(bb.equals("Jakub") && bb.equals("jakub")){
                    System.out.println("Pewnyś?");
                    cc = odczyt.nextLine();
                        if(cc.equals("nie") || cc.equals("Nie") || cc.equals("Chyba") || cc.equals("Nie.") || cc.equals("nie.") || cc.equals("chyba") || cc.equals("Chyba.")){
                            System.out.println("Nie jesteś pewny.");
                            cc = odczyt.nextLine();
                        }else if(cc.equals("Tak") && cc.equals("tak") && cc.equals("tak.") && cc.equals("Tak.") && cc.equals("Jasne") && cc.equals("jasne") && cc.equals("Jestem") && cc.equals("jestem")){
                            System.out.println("Jesteś pewny.");
                            cc = odczyt.nextLine();
                        }else{
                            System.out.println("Nie wiadomo czy jesteś pewny");
                            cc = odczyt.nextLine();
                        }
                }else{
                    System.out.println("Wybrało else");
                    if(cc.equals("nie") && cc.equals("Nie") && cc.equals("Chyba") && cc.equals("Nie.") && cc.equals("nie.") && cc.equals("chyba") && cc.equals("Chyba.")){
                        System.out.println("Nie jesteś pewny.");
                    }else if(cc.equals("Tak") && cc.equals("tak") && cc.equals("tak.") && cc.equals("Tak.") && cc.equals("Jasne") && cc.equals("jasne") && cc.equals("Jestem") && cc.equals("jestem")){
                        System.out.println("Jesteś pewny.");
                    }else{
                        System.out.println("Nie wiadomo czy jesteś pewny");
                    }
                }*/



        }while (!bb.equals("Jakub") && !bb.equals("jakub") && !bb.equals("tak") && !bb.equals("Tak"));


        System.out.println("Witam "+bb);
        String ee;
        String cc;
        String D, O, P, M, suma,T, t;
        D = "Dodawanie";
        O = "Odejmowanie";
        P = "Dzielenie";
        M = "Mnożenie";
        suma = "";
        Boolean jeszczeraz;
        Boolean True = null;
        Boolean False = null;
        jeszczeraz = True;
        T = "T";
        t = "t";


        while(jeszczeraz == True) {
            System.out.println("Próba trzecia 'liczby'. \nPodaj pierwszą liczbę.");


            a1 = odczyt.nextDouble();

            System.out.println("Podaj drugą liczbę");
            a2 = odczyt.nextDouble();

            System.out.println("Mamy dwie liczby. "+a1+" i "+a2+". Co chcesz z nimi zrobić? \n- Dodać: D; \n- Odjąć: O; \n- Podzielić: P; \n- Pomnożyć: M;");
            cc = odczyt.nextLine();
            cc = odczyt.nextLine();

            if (cc.equals("D")) {
                System.out.println("Wybrałeś dodawanie.");
                a3 = (a1 + a2);
                suma = (a1 + "+" + a2 + "=" + a3);
            } else if (cc.equals("O")) {
                System.out.println("Wybrałeś odejmowanie.");
                a3 = (a1 - a2);
                suma = (a1 + "-" + a2 + "=" + a3);
            } else if (cc.equals("P")) {
                System.out.println("Wybrałeś Dzielenie.");
                a3 = (a1 / a2);
                suma = (a1 + "/" + a2 + "=" + a3);
            } else if (cc.equals("M")) {
                System.out.println("Wybrałeś Mnożenie.");
                a3 = (a1 * a2);
                suma = (a1 + "*" + a2 + "=" + a3);
            } else {
                System.out.println("Nie wybrałeś nic. Tuman. Jeszcze raz");
                cc = odczyt.nextLine();
            }
            System.out.println(suma);
            System.out.println("Jeszcze raz? T/t");
            cc = odczyt.nextLine();

            if (cc.equals("T")){
                jeszczeraz=False;
            }
            if (cc.equals("t")){
                jeszczeraz=False;
            }

        }


    }
}

package Other;

public class Webinaria {

    public static void main(String[] args) {
        // konstruktor domyślny

        Osoba michal = new Osoba("Michał", "Makaruk", 13);

        michal.name = "Michał";
        michal.surname = "Makaruk";
        michal.age = 13;
        System.out.println(michal.name+" "+michal.age);
        Osoba jola = new Osoba();
        jola.name = "Ania";
        jola.surname = "Kowalska";
        System.out.println(jola.name+" "+jola.surname);

        Car myCar = new Car();
        myCar.brand = "BMW";
        myCar.model = "X5";
        myCar.owner = michal;

        System.out.println(myCar.brand+" "+myCar.model);
        System.out.println("Wlaściciel "+myCar.owner.fullName());
    }
}

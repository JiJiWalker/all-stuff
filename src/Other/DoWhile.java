package Other;

/**
 * Created by jakub on 15.06.2016.
 */
public class DoWhile {
    public static void main(String[] args){
        int licznik = 10;

        System.out.println("Zaczynamy");

        do{
            System.out.println(+licznik); // wyświetli się przynajmniej raz <--- nawet jeśli nie spełnia warunku
            licznik--;
        }
        while(licznik>0);
        System.out.println("Koniec petli");
    }
}

package Other;

/**
 * Created by jakub on 29.08.16.
 */
public class ArraysApp {
    public static void main(String[] args){

        String[] words = new String[3];

        words[0] = "What";
        words[1] = "are";
        words[2] = "You";

        System.out.println(words[0]);

        String[] names = {"name1", "name2", "name3", "name4"};

        for(String name: names){ // cała tablica names zamienia się na jedną zmienna name
            System.out.println(name);
        }

        String fruit = null; // fruit = nic

        System.out.println(fruit);

        String[] fruits = new String[2]; // nie wiem czemu, ale fruits = fruit.

        System.out.println(fruits[0]);

        fruits[1] = "two";  // dopiero po określeniu tablicy jako "two" zmienia ona wartość z null na tę określona

        System.out.println(fruits[1]);



    }
}

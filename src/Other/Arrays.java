package Other;

/**
 * Created by jakub on 12.08.16.
 */
public class Arrays {
    public static void main(String[] args){

        String[] words = new String[5];

        words[0] = "Why";
        words[1] = "do";
        words[2] = "I";
        words[3] = "need";
        words[4] = "you?";

        System.out.println(words[2]);

        String[] names = {"Jakub", "Ania", "Andrzej", "Beata", "Piotrek"};

        for (String word: words){
            System.out.println(word);
        }

        System.out.println(names[3]);

        int[] numbers = new int[3];

        numbers[0] = 1;
        numbers [1] = 2;
        numbers [2] = 4;

        for (int i=0; i<numbers.length; i++){
            System.out.println(numbers[i]);
        }
    }
}

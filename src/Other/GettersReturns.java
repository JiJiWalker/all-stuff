package Other;

class Person1 {
    String name;
    int age;

    void speak() {
        System.out.println("My name is "+name+".");
    }

    //void retirementYearsLeft() {  to stosowałem zanim użyłem return
        //int yearsLeft = 65 - age;
    int retirementYearsLeft() { //do tego dodałem int bo zamieniłem metodę na wartość którą chce zwrócić.
        int yearsLeft = 65 - age;

        return yearsLeft;
    }

    public String getName() {  //getter
        return name;
    }

    public int getAge() {
        return age;
    }
}
public class GettersReturns {

    public static void main (String[] args){

        Person1 person1 = new Person1();
        person1.name = "Jack";
        person1.age = 30;

        // person1.speak(); //osoba mówi to co jest na górze. Po wprowadzeniu getter niepotrzebne.
        int years = person1.retirementYearsLeft(); // zwraca metodę z latami.

        System.out.println("I'll be working for next "+years+" years.");

        int age = person1.getAge();
        String name = person1.getName();

        System.out.println("My new name after getter is "+name);
        System.out.println("I'll be working for the next "+age);
    }
}

package Other;

/**
 * Created by jakub on 06.09.16.
 */
class Machine {

    public Machine() { // nazwa konstruktora w metodzie jest pisana dużą literą i jest taka sama jak klasa
        System.out.println("Constructor running!");

    }
}
public class Constructors {
    public static void main(String[] args) {

        Machine machine1 = new Machine();
    }
}

package Other;

/**
 * Created by jakub on 06.09.16.
 */
class Robot {
    public void speak(String text) {
        System.out.println(text);
    }

    public void jump(int hight) {
        System.out.println("Jumping: "+hight);
    }

    public void move(String direction, double distance) {
        System.out.println("Proud Jack moving to "+direction+". He past "+distance+" meters.");
    }
}
public class MethodParametrs {

    public static void main(String[] args) {
        Robot jack = new Robot();

        jack.speak("Hi, I'm Jack");
        jack.jump(10);
        jack.move("Home", 12.3);

        String greetings = "Hello there";
        jack.speak(greetings); //zamienia pierwotne jack.speak na greetings
    }
}

package Other;

/**
 * Created by jakub on 29.08.16.
 */
public class MultiArrays {
    public static void main(String[] args){

        int[] values = {1, 2, 3, 444, 12};

        System.out.println(values[3]);

        int[][] multi = {
                {1,2,3,4,}, //tablica [0] i pod tablice [3]
                {33,22,44},
                {123,321,231,111,222},
        };

        System.out.println(multi[2][2]);
        System.out.println(multi[1][1]);

        String[][] wyrazy = new String[1][1];

        wyrazy[0][0] = "Siema";

        System.out.println(wyrazy[0][0]);

        for(int i=0; i<multi.length; i++){
            for(int j=0; j<multi[i].length; j++){
                System.out.print(multi[i][j] + "\t"); //bez ln daje wszystko w jednej linijce|| Tylda z "+" i w nawiasach
            }
            System.out.println(); // kiedy dodam puste String bez tyldy (\t):     Kiedy dodam z tyldą (\t)
                                    // 1234                                       1  2  3  4
                                    // 332244 bez przerw, pod sobą                33  22  44  są przerwy,
                                    // 123321231111222                            123  321  231  111  222
        }

        String[][] words = new String[2][];

        System.out.println(words[0]);

        words[0] = new String[3];

        words[0][1] = "Cześć";

        System.out.println(words[0][1]);

    }


}
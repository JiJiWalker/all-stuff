package Other;

class Person{

    // Klasy mogą zawierać
    String name;
    int age;
    public String surname;

    // 1. Data (dane)
    // 2. Subroutines (metody)
    void speak() {
        System.out.println("Ma name is "+name+" and I'm "+age+" years old.");
    }

    public String fullName() {
        return name+" "+surname;
    }
}


public class ClassObject {
    public static void main(String[] args){

        Person person1 = new Person();
        person1.name = "Jack Black";
        person1.age = 30; //zmienne dodajemy bez nawiasóœ
        person1.speak();// methody dodajemy z nawiasami()

        Person person2 = new Person();
        person2.name = "Jack White";
        person2.age = 35;

        System.out.println(person1.name);

    }
}

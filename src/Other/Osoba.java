package Other;

/**
 * Created by jakub on 13.09.16.
 */
public class Osoba {
    String name;
    String surname;
    int age;

    String fullName() {
        return name+" "+surname;
    }

    public Osoba () {

    }

    public Osoba (String name, String surname, int age){
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

}

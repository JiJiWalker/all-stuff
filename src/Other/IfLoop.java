package Other;

/**
 * Created by jakub on 28.07.16.
 */
public class IfLoop {
    public static void main(String[] args){
        int pentla = 5;

        while (true){ // gdyby nie if true leciało by w nieskończoniość
            System.out.println("Przekroczono standard!" +pentla);

            if (pentla == 7){
                System.out.println("Siódma brama. ");
                break;
            }

            pentla++;
            System.out.println("Dodaję...");
        }

    }
}

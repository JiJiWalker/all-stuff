package Other;

/**
 * Created by jakub on 15.06.2016.
 */
public class BooleanTrial {
    public static void main(String[] args){
        int a = 5;
        int b = 10;
        boolean prawda = b>a;
        boolean falsz = a>b;
        boolean porownanie = a==b;
        boolean koniunkcja = (a<b) && (a!=b);

        System.out.println(prawda);
        System.out.println(falsz);
        System.out.println(porownanie);
        System.out.println(koniunkcja);
    }
}

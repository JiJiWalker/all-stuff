package Other; /**
 * Created by jakub on 15.06.2016.
 */
import java.util.Scanner;

public class PetlaAd {
    public static void main(String[] args){
        Scanner odczyt = new Scanner(System.in);
        int zegar;

        System.out.println("Ustaw licznik na: ");
        zegar = odczyt.nextInt();

        while(zegar>0){
            System.out.println("Bomba wybuchnie za: "+zegar);
            zegar--;
        }
        System.out.println("Bang!");
    }
}

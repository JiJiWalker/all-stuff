package Other;

/**
 * Created by jakub on 26.07.16.
 */
public class Funkcje_2 {
    public static void main(String[] args){
        double liczba = 9;
        int b = 3;
        double pierwiastek = Math.sqrt(liczba);
        double potega = Math.pow(liczba, b);

        System.out.println("Pierwiastek z "+liczba+" wynosi "+pierwiastek+".");
        System.out.println("Liczba "+liczba+" podniesiona do potęgi "+b+" wynosi "+potega+".");
    }
}

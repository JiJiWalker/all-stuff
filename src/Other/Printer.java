package Other;

/**
 * Created by jakub on 02.10.16.
 */
public class Printer {
    private String text = "Włącz to.";

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void printText() {
        System.out.println(text);
    }
}

package Other;

public class Kalkulator {
	public static void main(String[] args){
		int a = 5; //mo�e tu by� spacja (a = 5) a mo�e jej nie by� a=5
		int b = 3;
		int c = 5; //double oznacza liczny po przecinku
		/* Po zmiane double na int wnioski:
		 * jedna zmienna z DOUBLE daje liczne z przecinkiem
		 * bez DOUBLE daje bez 
		 * int c = 5 czyli 5
		 * double c = 5 czyli 5.0
		 * tak samo wynik dzielenia
		 * int c/b = 1
		 * double c/b = 1,6666666666666667
		 */
		System.out.println("a+b = "+(a+b));
		System.out.println(c);
		System.out.println("c/b = "+(c/b));
		/* Wyra�enie niebieskie w nawiasie
		   oznacza to co si� poka�e ni�ej
		   czyli a+b =
		   Natomiast +(a+b) (mo�e by� bez +) to wynik*/

	}

}

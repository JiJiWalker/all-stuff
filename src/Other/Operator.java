package Other;

public class Operator {
	public static void main(String[] args){
		int a = 10;
		int b = 3;
		int c = (a+b)/3; //"c" to wynik 13 podzieli� na 3 = 4
		double d = 4.0;
		double e = 7.0;
		int f = a+b++; //a+++b c=a+b i a=a+1
		int g = a+++b;
		int h = a-b--;
		int i = a---b;
		
		/*
		 c = a-b
		 c = a/b
		 c = a*b
		 c = a%b  reszta z dzielenia
		 */
		
		System.out.println(c);
		System.out.println(d/c);
		System.out.println(e/d);
		System.out.println(f);
		System.out.println(g);
		System.out.println(h);
		System.out.println(i);
	}

}

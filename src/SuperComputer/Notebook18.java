package SuperComputer;

/**
 * Created by jakub on 05.12.16.
 */
public class Notebook18 extends Computer18 {

    private int batteryCapacity;

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    //konstruktor odziedziczony poprzez konstrukcję super.
    public Notebook18(double cpuTemperature, int ramMemory, int batteryCapacity) {
        super(cpuTemperature, ramMemory);
        setBatteryCapacity(batteryCapacity);
    }

    public void coolDown() {
        super.coolDown(); //super wywołuje metodę z nadklasy po kropce.
        turboCool(); // dodatkowo dodaje swoją metodę.
    }

    public void turboCool() {
        setCpuTemperature(getCpuTemperature()-2);
    }
}

package SuperComputer;

/**
 * Created by jakub on 05.12.16.
 */
public class Computer18 {

    private double cpuTemperature;
    private int ramMemory;

    public double getCpuTemperature() {
        return cpuTemperature;
    }

    public void setCpuTemperature(double cpuTemperature) {
        this.cpuTemperature = cpuTemperature;
    }

    public int getRamMemory() {
        return ramMemory;
    }

    public void setRamMemory(int ramMemory) {
        this.ramMemory = ramMemory;
    }

    //konstruktor

    public Computer18(double cpuTemperature, int ramMemory) {
        this.cpuTemperature = cpuTemperature;
        this.ramMemory = ramMemory;
    }

    public void coolDown() {
        setCpuTemperature(getCpuTemperature()-1);
    }
}

/**
 * Created by jakub on 20.12.16.
 */
public class EqualsHuman {

    private String name;
    private String name2;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public EqualsHuman(String name, String name2) {
        this.name = name;
        this.name2 = name2;
    }

    /*@Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EqualsHuman)) return false;

        EqualsHuman that = (EqualsHuman) o;

        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
        return getName2() != null ? getName2().equals(that.getName2()) : that.getName2() == null;
    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getName2() != null ? getName2().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "First Name of the Names "+getName()+"\n"+
                "Second Name of the Names "+getName2()+"\n";
    }

}

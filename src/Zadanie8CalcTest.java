/**
 * Created by jakub on 30.09.16.
 */
public class Zadanie8CalcTest {
    public static void main(String[] args){

        int a = 5;
        int b = 10;
        int c = 15;

        Zadanie8Calculator op = new Zadanie8Calculator();
        
        op.add(1, 3);
        op.add(1, 2, 3);
        op.add(a, b, c);
        System.out.println(op.add(1, 3));
        System.out.println(op.add(3, 4));
        System.out.println(op.add(a, b, c));
        System.out.println(op.add(1, 2, 3));
        System.out.println(op.add(2, 3, 2));
        op.add(50.0, 32.542);
        System.out.println(op.add(50.0, 32.542));

        op.substract(a, b, b);
        System.out.println(op.substract(a, b, b));
        op.substract(a, b);
        System.out.println(op.substract(a, b));
        op.substract(1.0, 2.0, 3.0);
        System.out.println(op.substract(1.0, 2.0, 3.0));





    }
}

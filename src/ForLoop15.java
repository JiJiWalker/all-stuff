/**
 * Created by jakub on 31.10.16.
 */
public class ForLoop15 {
    public static void main(String[] args) {
        int[] numbers = new int[10];
        // uzupełniamy tablicę
        for (int i=0; i<numbers.length; i++) {
            numbers[i] = i+1;
            System.out.println("Liczba: " +numbers[i]);
        }
        System.out.println(numbers[2]);
    }
}

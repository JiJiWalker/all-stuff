package CarShop.logic;

import CarShop.CarFactory7;
import CarShop.data.Car6;

/**
 * Created by jakub on 24.09.16.
 */
public class CarShop6 {
    public static void main(String[] args) {

        // zmienone już na set i get. set ustawiam nazwy a get te nazwy odbieram.
        Car6 audi = new Car6();
        audi.setBrand("Audi");
        audi.setModel("A4");
        audi.setDoors(5);
        audi.setCarColor("Czarny");
        audi.setWheelsColor("Srebrny");
        audi.setTiresColor("Czarny");

        String audiInfo = "Marka: "+audi.getBrand()+"\n"
                        +"Model: "+audi.getModel()+"\n"
                        +"Dzwi: "+audi.getDoors()+"\n"
                        +"Kolor nadwozia: "+audi.getCarColor()+"\n"
                        +"Kolor felg: "+audi.getWheelsColor()+"\n"
                        +"Kolor opon: "+audi.getTiresColor()+"\n";
        System.out.println("Wybrałeś następujący samochód: "+"\n");
        System.out.println(audiInfo);

        // Czy audi jest samochodem?
        boolean isCar = audi instanceof Car6;
        System.out.println(isCar); //true

        // po dodaniu metody do carfaktory i car6
        CarFactory7 cf = new CarFactory7();
        Car6 bmw = cf.createCar6("BMW", "X5", 5, "Czarny", "Srebrny", "Czarny");
        Car6 vwpassat = cf.createCar6("Volkswagen", "Passat", 5, "Bordowy Metaliczny", "Srebrny", "Czarny");
        Car6 porshe = cf.createCar6("Porshe", "Cayene", 3, "Czarny chrom", "Czarny", "Czarny");

        bmw.printInfo();
        vwpassat.printInfo();
        porshe.printInfo();

        //po dodaniu konstruktora do car6
        Car6 maluch = new Car6("Fiat", "Mały", 2, "Czerwony", "Opcjonalne Szare", "Czarne");
        maluch.printInfo();

        // użyto konstruktora kopiującego auto. audi2 to audi1
        Car6 audi2 = new Car6(audi);
        audi2.printInfo();

    }
}

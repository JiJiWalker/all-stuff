package CarShop;

import CarShop.data.Car6;

/**
 * Created by jakub on 27.09.16.
 */
public class CarFactory7 {


    public Car6 createCar6(String carBrand, String carModel, int doorsNumber,
                    String color, String wheels, String tires) {

        Car6 car = new Car6();

        car.setBrand(carBrand);
        car.setModel(carModel);
        car.setDoors(doorsNumber);
        car.setCarColor(color);
        car.setWheelsColor(wheels);
        car.setTiresColor(tires);

        return car;
    }
}

package CarShop.data;

/**
 * Created by jakub on 24.09.16.
 */
public class Car6 {

    private int doors;
    private String carColor;
    private String wheelsColor;
    private String tiresColor;
    private String brand;
    private String model;

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getWheelsColor() {
        return wheelsColor;
    }

    public void setWheelsColor(String wheelsColor) {
        this.wheelsColor = wheelsColor;
    }

    public String getTiresColor() {
        return tiresColor;
    }

    public void setTiresColor(String tiresColor) {
        this.tiresColor = tiresColor;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Car6() {

    }

    public Car6(String carBrand, String carModel, int doorsNumber,
         String color, String wheels, String tires) {

        this.setBrand(carBrand);
        this.setModel(carModel);
        this.setDoors(doorsNumber);
        this.setCarColor(color);
        this.setWheelsColor(wheels);
        this.setTiresColor(tires);

    }

    public Car6(Car6 carToCopy) {
        this(carToCopy.getBrand(), carToCopy.getModel(), carToCopy.getDoors(),
                carToCopy.getCarColor(), carToCopy.getWheelsColor(), carToCopy.getTiresColor());
    }

    public void printInfo() {
        String info = "Wybrałeś: "+"\n"+"\n"
                    +"Marka: "+brand+"\n"
                    +"Model: "+model+"\n"
                    +"Dzwi: "+doors+"\n"
                    +"Kolor nadwozia: "+carColor+"\n"
                    +"Kolor felg: "+wheelsColor+"\n"
                    +"Kolor opon: "+tiresColor+"\n";

        System.out.println(info);
    }

}

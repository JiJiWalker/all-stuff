package AnimalProject;

/**
 * Created by jakub on 04.12.16.
 */
public class Zoo {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.color = "Czarny";

        Bird bird = new Bird();
        bird.color = "Niebieski";

        System.out.println("Zwierzęta dają głoś: ");
        System.out.print("Kot ");
        cat.makeSound();
        System.out.print("Ptak ");
        bird.makeSound();
    }
}

package TestoweUdemy.Udemy;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Jakub on 11.03.2017.
 */
public class S9L55ArrayChallenge {

    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[] challengeArray = getIntegers(5);
        for (int i=0; i<challengeArray.length; i++) {
            System.out.println("To the index "+i+" you assigned "+challengeArray[i]);
        }
        int[] sorted = sortIntegers(challengeArray);
        printArray(sorted);
        printArray(challengeArray);
        sortArray(challengeArray);
        System.out.print("Reversed array numbers: ");
        reverseSortArray(challengeArray);

    }

//    metoda pozwalająca na przypisanie 'number' do tablicy.
//    po wpisaniu 'number', możemy przypisać taką ilość liczb jaką przypisaliśmy do tabliby.
    public static int[] getIntegers(int number) {
        System.out.println("Enter numbers"+"\r");
        int[] values = new int[number];
        for (int i=0; i<values.length; i++) {
            values[i] = sc.nextInt();
        }
        return values;
    }

//    metoda drukuje aktualną tablice i jej indeksy
    public static void printArray(int[] array) {
        for (int i=0; i<array.length; i++) {
            System.out.println(i+" index is "+array[i]);
        }
    }

//    metoda sortuje elemnty tablicy za pomocą klasy Array i metody sort.
//    elementy są drukowane za pomocą metody toString.
    public static void sortArray(int[] array) {
        Arrays.sort(array);
        System.out.println("Sorted numbers in array "+Arrays.toString(array));
    }

//    metoda odwraca kolejnośc sortowania tablicy (od największego do najmniejszego)
//    nie umiem w tym przypadku zrobić z metody void zwykłej
    public static void reverseSortArray(int[] array) {
//        int sum = 0;
        for (int i = array.length -1; i>=0; i--) {
            System.out.print(array[i] + " ");
        }
//        return sum;
    }

//    Kod z Udemy napisany przez prowadzącego, który też zwraca od największego do najmniejszego
//    przez to że zarówno w metodzie jak i parametrze jest '[]' można utworzyć nową tablicę dzięki tej metodzie
//    oraz w parametrze podać tablicę
//    int[] tablica = sortIntegers[wcześniejUtworzonaTablica];
    public static int[] sortIntegers(int[] array) {
        int[] sortedArray = new int[array.length];
        for (int i=0; i<array.length; i++) {
            sortedArray[i] = array[i];

//     Pondato, powyższe trzy linijki które kopiują indexy do nowej tablicy można zastąpić jedną linijką dzięki
//     Klasie Arrays.
//     int[] sortedArray = Arrays.copyOf(array, array.length);
        }
        boolean flag = true;
        int temp;
        while (flag) {
            flag = false;
            for (int i=0; i<sortedArray.length-1; i++) {
                if (sortedArray[i] < sortedArray[i+1]) {
                    temp = sortedArray[i];
                    sortedArray[i] = sortedArray[i+1];
                    sortedArray[i+1] = temp;
                    flag = true;
                }
            }
        }

        return sortedArray;
    }


}

package TestoweUdemy.Udemy;

import java.util.Scanner;

/**
 * Created by Jakub on 11.03.2017.
 */
public class S9L56ArrayListDLC {
    private static Scanner sc = new Scanner(System.in);
    private static int[] baseData = new int[10];

    public static void main(String[] args) {
        System.out.println("Enter 10 integers:");


    }

    public static void getInput() {
        for (int i=0; i<baseData.length; i++) {
            baseData[i] = sc.nextInt();
        }
    }

    public static void printArray(int[] arr) {
        for (int i=0; i<baseData.length; i++) {
            System.out.println("Index "+i+" equals "+arr[i]);
        }
        System.out.println();
    }

//    metoda która biernie powiększa tablicę z 10 (ustawione na górze) na 12
//    tworzy drugą tablicę do której przypisujemy pierwszą.
//    tworzymy operację na oryginalnej tablicy, a później przypiujemy do drugiej
    public static void resizeArray() {
        int[] original = baseData;

        baseData = new int[12];
        for (int i=0; i<original.length; i++) {
            baseData[i] = original[i];
        }
    }
}

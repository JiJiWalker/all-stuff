package TestoweUdemy.Udemy;

import java.util.ArrayList;

/**
 * Created by Jakub on 11.03.2017.
 */
public class S9L56ListAndArrayListP1 {
    private ArrayList<String> groceryList = new ArrayList<>();

//    metoda która dodaje element do listy. Prosta ze względu na gotowe już metody w klasie Array
    public void addGrocery(String item) {

        groceryList.add(item);
    }

//    metoda, która drukuje elementy w liście grocery.
//    najpierw podajemy metodą '.size' ilość elemntów w tablicy (zwraca nam ona zapełnione indeksy)
//    póżniej przez pętlę for i warunek grocery.size, wyświetlamy to co mamy w tablicy.
    public void printGroceryList() {
        System.out.println("You have " + groceryList.size() + " items in your grocely list.");
        for (int i=0; i<groceryList.size(); i++) {
            System.out.println((i+1) + ". " + groceryList.get(i));
        }
    }

//    metoda, która modyfikuje konkretny element tablicy.
//    po to podaliśmy w parametrach metody dwa elementy int i String, ponieważ w metodzie .set z klasy Array
//    podajemy pozycję elemtnu który chcemy zmodyfikować oraz nazwę nowego elementu.
    public void modifyGroceryItems(int position, String newItem) {
        groceryList.set(position, newItem);
        System.out.println("Grocery items " + (position+1) + " has been modified.");
    }

//    metoda, która usuwa jedną pozycję z listy.
    public void removeGroceryItem(int position) {
        String theItem = groceryList.get(position);
        groceryList.remove(position);
    }

    public String findItem(String searchItem) {
//        boolen exist = groceryList.contains(searchItem);

        int position = groceryList.indexOf(searchItem);
        if (position >=0) {
            return groceryList.get(position);
        }
        return null;
    }
}

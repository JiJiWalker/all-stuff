package TestoweUdemy;

import java.util.Scanner;

/**
 * Created by Jakub on 11.03.2017.
 */
public class ArrayMethod {

//    aby użyć w metodzie statycznej Scanner musi też być statyczny.
//    deklaracja była potrzebna oddzielnie ze względu na żółt błąd. Normalnie wszystko było w jednej linijce.
//    Deklaracja została zrobiona automatycznie przez podpowiedź IntelliJ
    private static Scanner sc;

    static {
        sc = new Scanner(System.in);
    }

    public static void main(String[] args) {
        int[] myIntArray = getInteger(5);
//        ta pętla pokazuje to co już zostało przypisane przez metodę.
        for (int i=0; i<myIntArray.length; i++) {
            System.out.println("Index "+i+" is "+myIntArray[i]);
        }
        System.out.println("Sum of second method is "+getAverage(myIntArray));
        System.out.println("Length of array is: "+getLenght(myIntArray));

    }

//    metoda ta najpierw przypisuje parametr metody 'number' wewnętrznej zmiennej
//    póżniej przez pętle zmienna jest traktowana jako tablica jednak zamiast 'i+1"
//    jest metoda 'sc.nextInt();'. Dzięki temu w powyższym przykładzie, gdzie metoda jest już wywoływana
//    na wpisanych liczbach nie są dokonywane żadne zmienne. Na samym końcu zwracamy zmienna 'values'.
    private static int[] getInteger(int number) {
        System.out.println("Enter "+number+" integer values.");
        int[] values = new int[number];

        for (int i=0; i<values.length; i++) {
            values[i] = sc.nextInt();
        }
        return values;
    }

//    Trzeba zadeklarować zmienną sum.
//    Skoro parametrem będzie cała pętla, trzeba w nawiasach podać pętle 'int[] array'.
    private static double getAverage(int[] array) {
        int sum = 0;
        for (int i =0; i<array.length; i++) {
            sum += array[i];
        }
        return sum / array.length;
    }

//    To zrobiłem sam. Metoda ma zwracać długość pętli.
    private static int getLenght(int[] array) {
        int sum = 0;
        for (int i =0; i<array.length; i++) {
            sum = array.length;
        }
        return sum;
    }
}

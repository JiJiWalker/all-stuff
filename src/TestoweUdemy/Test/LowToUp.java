package TestoweUdemy.Test;

import java.util.Arrays;

/**
 * Created by Jakub on 05.06.2017.
 */
public class LowToUp {
    public static void main(String[] args) {
        String one = "BlablA";
        toUpperFromLow(one);
    }

    public static String toUpperFromLow(String str) {
        char[] chars = str.toCharArray();

        for (int i=0; i<str.length(); i++) {
            char x = chars[i];
            if (Character.isUpperCase(x)) {
                chars[i] = Character.toLowerCase(x);
            } else if (Character.isLowerCase(x)) {
                chars[i] = Character.toUpperCase(x);
            }
        }
        System.out.println(Arrays.toString(chars));
        return new String(chars);
    }
}

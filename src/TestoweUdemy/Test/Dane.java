package TestoweUdemy.Test;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Jakub on 24.02.2017.
 */
public class Dane {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int jeden = 0;
        String dwa = null;
        String elo;

        elo = sc.nextLine();

        System.out.println("Wpisz liczbę i słowo:");
        boolean readComplete = false;



        while (!readComplete) {
            try {
                System.out.println("Liczba");
                jeden = sc.nextInt();
                sc.nextLine();
                System.out.println("Słowo");
                dwa = sc.nextLine();
                readComplete = true;
            } catch (InputMismatchException e) {
                System.out.println("Niepoprawna wartość.");
                sc.nextLine();
            }
        }
        System.out.println("Wybrałeś "+jeden+" "+dwa);
    }
}

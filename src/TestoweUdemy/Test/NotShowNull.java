package TestoweUdemy.Test;

/**
 * Created by Jakub on 14.06.2017.
 */
public class NotShowNull {
    private String firstName;
    private String secondName;
    private int age;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public NotShowNull(String firstName, String secondName, int age) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Pokaż: "+"\n"+
                "Imię: "+ firstName +"\n+" +
                "Nazwisko: "+ secondName+"\n"+
                "Wiek: "+age;
    }
}

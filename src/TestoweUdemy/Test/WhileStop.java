package TestoweUdemy.Test;

import java.util.Scanner;

/**
 * Created by jakub on 21.03.17.
 */
public class WhileStop {
    public static void main(String[] args) {
        int prawda = 2;
        Scanner sc = new Scanner(System.in);
        int flag;


        System.out.println(Math.abs(100));
        System.out.println("Wpisz liczbę. 2 kontunuuje pętle");

        flag = sc.nextInt();

        while (flag == prawda) {
            System.out.println("Warunek jest spełniony.");
            flag = sc.nextInt();
        }
        System.out.println("Warunek nie został spełniony");
        // czyli nie = 2

    }
}

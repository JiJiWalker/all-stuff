package TestoweUdemy.Test;

/**
 * Created by Jakub on 05.06.2017.
 */
public class InvertInt {
    public static void main(String[] args) {
    int[] i = new int[] {1, 2, 3, 4, 5} ;
    invert(i);
        for (int e: i
             ) {
            System.out.println(e);
        }

    }

    public static int[] invert(int[] array) {

       for (int i = 0; i<array.length; i++) {
           array[i] = -array[i];
       }
        return array;
    }
}

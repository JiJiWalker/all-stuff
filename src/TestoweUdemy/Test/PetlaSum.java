package TestoweUdemy.Test;

/**
 * Created by jakub on 19.03.17.
 */
public class PetlaSum {
    public static void main(String[] args) {

        int[] numberArray = new int[20];
        int sum = 0;
        for (int i = 0; i < numberArray.length; i++) {
            numberArray[i] = i; // dodaje po kolei indexy.
            sum = sum + (i); // sumuje indexy

//            gdybym zrobił i+1 w powyższych dwóch linijkach, nie zaczynałoby się output od 0 tylko od 1.
            System.out.println("Liczby to " + numberArray[i] + " oraz " + sum);
        }
    }
}

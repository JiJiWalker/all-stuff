package TestoweUdemy.Test;

/**
 * Created by Jakub on 19.06.2017.
 */
public class TaskHelloGoodbye {
    public static void main(String[] args) {
        try {
            System.out.println("Hello world");
            System.exit(0);
        } catch (Exception e) {
            System.out.println("Ops");
        } finally {
            System.out.println("Goodbye world");
        }
    }
}

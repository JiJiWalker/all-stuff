package TestoweUdemy.Test;

/**
 * Created by Jakub on 27.02.2017.
 */
public class Petla {
    public static void main(String[] args) {

        int liczba[] = new int[20];

        for (int i=0; i<liczba.length; i++) {
            try {
                liczba[i] = i + 1;
                if (liczba[i] % 3 == 0 && liczba[i] % 5 == 0) {
                    System.out.println("Liczba podzielna przez 3 i 5. (" + liczba[i]+")");
                } else if (liczba[i] % 3 == 0) {
                    System.out.println("Liczba podzielna przez 3. (" + liczba[i] + ")");
                } else if (liczba[i] % 5 == 0) {
                    System.out.println("Liczba podzielna przez 5. (" + liczba[i] + ")");
                } else {
                    System.out.println(liczba[i]);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Nie mieści się w pętli");
            }
        }
    }
}

package TestoweUdemy.Test;

import javax.naming.Name;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Jakub on 19.06.2017.
 */
public class TaskName {
    private final String first, last;

    public TaskName(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public boolean equals(Object o) {
        if (!(o instanceof TaskName)) {
            return false;
        }
        TaskName n = (TaskName)o;
        return n.first.equals(first) && n.last.equals(last);
    }

    public static void main(String[] args) {
        Set<TaskName> s = new HashSet<>();
        s.add(new TaskName("Mickey", "Mouse"));
        System.out.println(s.contains(new TaskName("Mickey", "Mouse")));
        System.out.println(s.size());
    }
}

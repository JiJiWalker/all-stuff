package StringBuilderBuffer;

import java.util.Scanner;

/**
 * Created by jakub on 02.01.17.
 */
public class Zadanie22 {
    public static void main(String[] args) {

        int number;

        Scanner sc = new Scanner(System.in);
        System.out.println("Ile chcesz powtórzeń?");
        number = sc.nextInt();
        sc.nextLine();

        String[] words = new String[number];

        for (int i=0; i<number; i++) {
            System.out.println("Podaj słowo "+(1+i)+"/"+number);
            words[i] = sc.nextLine();
        }

        StringBuilder build = new StringBuilder();
        for (int i=0; i<number; i++) {
            build.append(words[i].charAt(words[i].length()-1));
        }

        System.out.println("Nowe słowo: "+build.toString());
        sc.close();
    }
}

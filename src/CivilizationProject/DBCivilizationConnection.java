package CivilizationProject;

import java.sql.*;

public class DBCivilizationConnection {

    public DBCivilizationConnection() {
    }

    public void showPopulation(ResultSet rs, Statement stmt) {
        try {
            rs = stmt.executeQuery("select * from peoples");
            while (rs.next()) {
                System.out.println("Personal ID: " + rs.getString(1)
                + " First name: " + rs.getString(2)
                + " Last name: " + rs.getString(3)
                + " Gender: " + rs.getString(4)
                + " Age: " + rs.getInt(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void addCivilian(Statement stmt, String first_name, String last_name, String gender, int age) {
        try {
            stmt.executeUpdate("insert into peoples values('"+first_name+"', '"+last_name+"', '"+gender+"', "+age+")");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("New Civilian added.");
    }

    Connection connect() {
        Connection conn;
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/civilization", "root", "rassieliq69");
            return conn;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

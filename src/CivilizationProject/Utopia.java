package CivilizationProject;

import java.sql.ResultSet;
import java.sql.Statement;

public class Utopia {
    public static void main(String[] args) {
        DBCivilizationConnection dbCivilizationConnection = new DBCivilizationConnection();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            dbCivilizationConnection.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Statement stmt;
        ResultSet rs;
        try {
            stmt = dbCivilizationConnection.connect().createStatement();
            rs = stmt.getResultSet();

            dbCivilizationConnection.addCivilian(stmt, "Luna", "Moon", "Female", 24);
            dbCivilizationConnection.addCivilian(stmt, "Vincent", "Vega", "Male", 26);
            //dbCivilizationConnection.showPopulation(rs, stmt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

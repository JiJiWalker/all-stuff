import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Jakub on 18.02.2017.
 */
public class TestTryMulti23 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numbers[] = new int[2];
        boolean error = true;

        while (error) {
            try {
                System.out.println("Podaj pierwszą liczbę:");
                numbers[0] = sc.nextInt();
                sc.nextLine();
                System.out.println("Podaj drugą liczbę:");
                numbers[1] = sc.nextInt();
                sc.nextLine();

                System.out.println("Którą wartość wyświetlić (1 albo 2)?");
                System.out.println(numbers[sc.nextInt()-1]);
                error = false;
            } catch (InputMismatchException e) {
                System.out.println("Nie podałeś liczby całkowitej. Spróbuj jeszcze raz.");
                //sc.nextLine(); ta metoda i poniższa została przeniesiona do bloku "finally". Wykonuje się on zawsze niezależnie czy błąd wystąpi czy też nie.
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Miało być 1 lub 2. Zacznijmy od nowa.");
                //sc.nextLine(); W związku z tym nextLine występuje za każym razem.
            } finally {
                sc.nextLine();
            }
        }
        sc.close();
    }
}

/**
 * Created by jakub on 20.09.16.
 */
public class Zmienne4 {
    public static void main (String[] dupa) {

        String firstname = "Jakub";
        String lastname = " Nowak";
        String fullname = firstname+lastname;
        byte age = 24;
        double weight = 185.5;
        final String pesel = "92109210992";

        System.out.println("Cześć "+fullname);
        System.out.println("Poniżej znajdziesz kilka informacji o sobie:");
        System.out.println("Twój PESEL to: "+pesel);
        System.out.println("Masz "+age+" lat.");
        System.out.println("Twój wzrost to "+weight+".");
    }
}

/**
 * Created by jakub on 19.10.16.
 */
public class MultiDimensional13 {
    public static void main(String[] args) {
        String[] firstNames = {"Karol", "Kasia", "Basia"}; //pojedyńcze tablice odnośnie imienia i nazwiska
        String[] lastName = {"Nowak", "Kowak", "Rowak"};

        String[][] firstLast = {firstNames, lastName}; // podwójna tablica określająca i imię i nazwisko

        System.out.println("Pierwsza osoba to "+firstLast[0][0]+" "+firstLast[1][0]);
        System.out.println("Pierwsza osoba to "+firstLast[0][2]+" "+firstLast[1][2]);
        System.out.println("Pierwsza osoba to "+firstLast[0][2]+" "+firstLast[0][2]);

        //chodzi o to, że [0][0] - pierwsze zero to która tablica, drugie zero to który wiersz
    }
}

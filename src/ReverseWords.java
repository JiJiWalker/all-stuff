import java.util.Scanner;

/**
 * Created by jakub on 06.01.17.
 */
public class ReverseWords {
    public static void main(String[] args) {

        /*Scanner sc = new Scanner(System.in);
        String source = "";

        for (String part: source.split(source = sc.nextLine())) {
            System.out.println(new StringBuilder(part).reverse().toString());
            System.out.println(source);
        }*/

        String source = "ksiądz";

        for (String part : source.split(" ")) {
            System.out.print(new StringBuilder(part).reverse().toString());
            System.out.print(" ");
        }
    }
}

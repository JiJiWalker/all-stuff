package Lesson24.ShapeInterface;

/**
 * Created by Jakub on 22.02.2017.
 */
public class ShapeCalculator {
    public static void main(String[] args) {

       Shape circle = new Circle(2);
       Shape rectangle = new Rectangle(2, 2);

        System.out.println("Prostokąt pole: "+rectangle.calculateArea());
        System.out.println("Prostokąt obwód: "+rectangle.calculatePerimeter());

        System.out.println("Prostokąt pole: "+circle.calculateArea());
        System.out.println("Prostokąt obwód: "+circle.calculatePerimeter());


    }
}

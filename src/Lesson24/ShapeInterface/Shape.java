package Lesson24.ShapeInterface;

/**
 * Created by Jakub on 22.02.2017.
 */
public interface Shape {
    double PI = 3.14;
    // public static final automatycznie jest przypisywane do zmiennych w klasie interface

    double calculateArea();
    double calculatePerimeter();
}

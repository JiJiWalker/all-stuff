package Lesson24.MultiExtends;

/**
 * Created by Jakub on 22.02.2017.
 */
public class ClassWithMoreInterfejs implements A, B {
    // Klasy też mogą dziedziczyć wiele interfejsów.

    // dwie identyczne metody są problematyczne bo nie wiadomo którą wykonać.
    // dzięki poniższej przesłoniętej metodzie uda wywołać się konkretną metodę.
    @Override
    public void printName() {
        System.out.println("Amphibia");
    }

    // aby odwołać się bezpośrednio do klasy A albo B
    // trzeba wywołać NazwaKlasy.super.nazwaMetody{};

    public void printName2() {
        System.out.println("Amphibia to trochę: ");
        A.super.printName();
        System.out.println(" a trochę");
        B.super.printName();

        // output - Amphibia to trochę A a trochę B
    }
}

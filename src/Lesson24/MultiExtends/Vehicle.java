package Lesson24.MultiExtends;

/**
 * Created by Jakub on 22.02.2017.
 */
public interface Vehicle {
    default public int speedUp(int speed) {
        return speed++;
    }

    //niby nie można dodawać metod nieabstrakcyjnych, a od Javy 8 można przy pomocy default.
    // jakbym się kiedyś zastanawiał po co są to po klasie dziedziczą metody abstakcyjne, które można zmienić
    // natomiast metody default (chyba) nie są dziedziczone.
}

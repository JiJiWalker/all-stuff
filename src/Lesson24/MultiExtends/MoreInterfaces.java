package Lesson24.MultiExtends;

/**
 * Created by Jakub on 22.02.2017.
 */
public interface MoreInterfaces extends A, B {
    @Override
    default void printName() {

    }
    // interfejsy mogą dziedziczyć wiele innych interfejsów w odróżnieniu od klas.
}

package Lesson24.VehicleAbstractClass;

/**
 * Created by Jakub on 22.02.2017.
 */
public abstract class Vehicle {
    private int speed;

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    //metoda abstrakcyjna
    public abstract void speedUp();
}

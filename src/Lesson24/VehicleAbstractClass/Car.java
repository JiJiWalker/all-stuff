package Lesson24.VehicleAbstractClass;

import Lesson24.VehicleAbstractClass.Vehicle;

/**
 * Created by Jakub on 22.02.2017.
 */
public class Car extends Vehicle {

    @Override
    public void speedUp() {
        setSpeed(getSpeed()+5);
    }
}

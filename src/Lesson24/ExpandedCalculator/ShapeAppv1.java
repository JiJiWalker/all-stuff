package Lesson24.ExpandedCalculator;

/**
 * Created by Jakub on 23.02.2017.
 */
public class ShapeAppv1 {
    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        final String AREA = "A";
//        final String PERIMETER = "P";

        ShapeMain circle = new CircleCalc(5);
        ShapeMain rectangle = new RectangleCalc(2, 4);
        ShapeMain triangle = new TriangleCalc(2, 3, 4, 4);

//        String operation;
//        System.out.println("Co chcesz obliczyć?");
//        operation = sc.nextLine();
//        try {
//            switch (operation) {
//                case AREA:
//
//            }
//        }

        System.out.println("Pole koła: "+circle.calculateArea());
        System.out.println("Obwód koła: "+circle.calculatePerimeter());

        System.out.println("Pole prostokąta: "+rectangle.calculateArea());
        System.out.println("Obwód prostokąta: "+rectangle.calculatePerimeter());

        System.out.println("Pole trójkąta: "+triangle.calculateArea());
        System.out.println("Obwód trójkąta: "+triangle.calculatePerimeter());

    }
}

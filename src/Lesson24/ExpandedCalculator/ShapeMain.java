package Lesson24.ExpandedCalculator;

/**
 * Created by Jakub on 23.02.2017.
 */
public interface ShapeMain {
    int TRIANGLE = 1;
    int CIRCLE = 2;
    int RECTANGLE = 3;
    double PI = 3.14; // zmienna PI

    double calculateArea(); // metoda licząca pole
    double calculatePerimeter(); // metoda licząca obwód
}

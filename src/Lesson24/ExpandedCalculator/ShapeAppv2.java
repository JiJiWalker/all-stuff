package Lesson24.ExpandedCalculator;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;

/**
 * Created by Jakub on 24.02.2017.
 */
public class ShapeAppv2 {
    public static void main(String[] args) {
        ShapeAppv2 szejp = new ShapeAppv2();

        SzejpCalculator shapecalc = new SzejpCalculator();
        ShapeMain shape = null;

        boolean readComplete = false;
        while (!readComplete) {
            try {
                szejp.printOptions();
                shape = shapecalc.createShape();
                readComplete = true;
            } catch (InputMismatchException e) {
                System.out.println("Wprowadziłeś dane w złym formacie.");
            } catch (NoSuchElementException e) {
                System.out.println("Wybrany identyfikator kształtu jest niepoprawny.");
            }
        }

        System.out.println(shape);
        shapecalc.closeScanner();
    }

    private void printOptions() {
        System.out.println("Wybierz figurę, dla której chcesz obliczyć pole i obwód: ");
        System.out.println(ShapeMain.RECTANGLE+" - prostokąt.");
        System.out.println(ShapeMain.CIRCLE+" - koło.");
        System.out.println(ShapeMain.TRIANGLE+" - trójkąt.");
    }
}

package Lesson24.ExpandedCalculator;

/**
 * Created by Jakub on 23.02.2017.
 */
public class CircleCalc implements ShapeMain {
    private double r;

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public CircleCalc(double r) {
        this.r = r;
    }

    @Override
    public double calculateArea() {
        return ShapeMain.PI*r*r;
    }

    @Override
    public double calculatePerimeter() {
        return 2* ShapeMain.PI*r;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Koło, ");
        sb.append("promień: "+r+", ");
        sb.append("Pole: "+calculateArea()+", ");
        sb.append("Obwód: "+calculatePerimeter());
        return sb.toString();
    }
}

/**
 * Created by jakub on 20.09.16.
 */
public class Zadanie4 {
    public static void main(String[] args) {
        String model = "Ford";
        String marka = " Mustang GT";
        int rocznik = 1969;
        double cena = 65.000;

        System.out.println("\tNa 40-ste urodziny kupię się samochód.");
        System.out.println("Będzie to "+model+" "+marka+" rocznik "+rocznik+".");
        System.out.println("Już dziś zamówiłem go za: "+cena);
    }
}

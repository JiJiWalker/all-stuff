//STEP 1. Import required packages
package DataBaseClass;
import java.sql.*;

public class DBLongerVer {
//    JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/sonoo";

//    Datavase credentials
    private static final String USER = "root";
    private static final String PASS = "rassieliq69";


    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
//      STEP 2. Register JDBC driver
        try {
            Class.forName("com.mysql.jdbc.Driver");

//            STEP 3. Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

//            STEP 4. Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT id, age, first, last FROM employees";
            ResultSet rs = stmt.executeQuery(sql);

//            STEP 5. Extract data from result set
            while (rs.next()) {
//                Retrieve by column name
                int id = rs.getInt("id");
                int age = rs.getInt("age");
                String first = rs.getString("first");
                String last = rs.getString("last");

//                Display values
                System.out.println("ID: "+id);
                System.out.println("Age: "+age);
                System.out.println("First: "+first);
                System.out.println("Last: "+last+"\n");

            }
//            STEP 6. Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
//          Handle errors for JDBC
        } catch (Exception e) {
            System.out.println(e);
//            Finally block used to close resources
        } finally {
            try {
                if (stmt!=null)
                    stmt.close();
            } catch (SQLException ignored) {}
            try {
                if (conn!=null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        System.out.println("Goodbye!");
    }
}

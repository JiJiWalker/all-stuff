package DataBaseClass;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBShorterVer {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/sonoo";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "rassieliq69";

    public static void main(String[] args) {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            // 1. Get a connection to database
            Connection myConn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);

            // 2. Create a statement
            Statement myStmt = myConn.createStatement();

            // 3. Execute SQL query
            ResultSet myRs = myStmt.executeQuery("select * from employees");

            // 4. Process the result set
            while (myRs.next()) {
                int id = myRs.getInt("id");
                int age = myRs.getInt("age");
                String first = myRs.getString("first");
                String last = myRs.getString("last");

                System.out.println("ID: "+id);
                System.out.println("Age: "+age);
                System.out.println("First: "+first);
                System.out.println("Last: "+last+"\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

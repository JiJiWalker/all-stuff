package DataBaseClass;

import java.io.IOException;
import java.sql.*;

public class DBShorterVer2 {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/demo";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "rassieliq69";

    public static void main(String[] args) {

        Connection myConn;
        PreparedStatement myStmt;
        ResultSet myRs;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            // 1. Get a connection to database
            myConn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);

            // 2. Prepare statement
            myStmt = myConn.prepareStatement("select * from employees where salary > ? and department=?");
            // chodzi o to, że pod znakami zapytania są poniższe stejtmenty. 1 i 2 oznacza pierwszą i drugą pozycję '?'

            // 3. Set the parameters
            myStmt.setDouble(1, 80000);
            myStmt.setString(2, "Legal");

            // 4. Execute SQL query
            myRs = myStmt.executeQuery();

            // 4. Process the result set
//            while (myRs.next()) {
//                int id = myRs.getInt("id");
//                String last = myRs.getString("last_name");
//                String first = myRs.getString("first_name");
//                String email = myRs.getString("email");
//                String department = myRs.getString("department");
//                double salary = myRs.getDouble("salary");
//
//                System.out.println("ID: "+id);
//                System.out.println("Last: "+last);
//                System.out.println("First: "+first);
//                System.out.println("Email: "+email);
//                System.out.println("Department: "+department);
//                System.out.println("Salary: "+salary+"\n");
//            }
            // 5. Display the result set
            display(myRs);

//
//          REUSE THE PREPARED STATEMENT: salary >25000, department - HR
//

            System.out.println("\n\nReuse the prepared statementL salary >25000, department - HR");

            // 6. Set the parameters
            myStmt.setDouble(1, 25000);
            myStmt.setString(2, "HR");

            // 7. Execute SQL query
            myRs = myStmt.executeQuery();

            // 8. Display the result set
            display(myRs);

        } catch (Exception e) {
            e.printStackTrace();
        }
//        finally {
//            if (myRs != null) {
//                myRs.close();
//            }
//
//            if (myStmt != null) {
//                myStmt.close();
//            }
//
//            if (myConn != null) {
//                myConn.close();
//            }
//        }
    }

    private static void display(ResultSet myRs) throws SQLException {
        while (myRs.next()) {
            String lastName = myRs.getString("last_name");
            String firstName = myRs.getString("first_name");
            double salary = myRs.getDouble("salary");
            String department = myRs.getString("department");

            System.out.printf("%s, %s, %.2f, %s\n", lastName, firstName, salary, department);
        }
    }
}

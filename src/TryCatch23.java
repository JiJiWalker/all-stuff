import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by jakub on 10.01.17.
 */
public class TryCatch23 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę:");
        int number = 0;

        boolean error = true;
        while (error) {

            try {
                number = sc.nextInt();
                error = false;
            } catch (InputMismatchException e) {
                System.err.println("Podana wartość jest nieprawidłowa."); // wyświetla komunikat na czerwono (err)
                sc.nextLine();
                //e.printStackTrace(); // dzieki tej metodzie wyświetla się to co normalnie przy błędzie, ale program nadal działa.
            }

        }

        System.out.println("Podałeś: "+number);
        sc.close();
    }
}

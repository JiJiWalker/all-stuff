import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Jakub on 13.02.2017.
 */
public class TestTry23 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        int number = 0;
        boolean error = true;

        while (error) {

            try {
                number = sc.nextInt();
                error = false;
            } catch (InputMismatchException e) {
                System.err.println("Podana wartość nie jest liczbą całkowitą, spróbuj jeszcze raz.");
                sc.nextLine();
                //e.printStackTrace(); kod który wywołuje wychodzący błąd.
            }
        }
        System.out.println("Podałeś " + number+". Dzięki.");
        sc.close();

    }
}

import java.util.Scanner;

/**
 * Created by jakub on 10.10.16.
 */
public class HelloName11 {
    public static void main(String[] args) {

        // w zasadzie jest to niepotrzebne.
        final String imie = "Władymir";
        final String imie2; //= "Bartek"
        final String imie3;

        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        //String name = "Krzysiek"

        switch (name) {
            case "Jakub":case "Kuba": // dodanie dwóch case z tym samym poleceniem
                System.out.println("Welcome Jakub");
                break;
            case "Krzysiek":
                System.out.println("Zdrastwujtie Kristoff");
                break;
            case "Bartek":
                System.out.println("Cześć Bartek");
                break;
            case imie:
                System.out.println("Witam Władymirze");
                break;
            default:
                System.out.println("#@%#*");
        }
    }
}

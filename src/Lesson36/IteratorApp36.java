package Lesson36;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Jakub on 20.04.2017.
 */
public class IteratorApp36 {
    public static void main(String[] args) {

        Map<String, String> map = new TreeMap<>();
        map.put("Kowalski", "Jan Kowalski - 39");
        map.put("Nowak", "Bartosz Nowak - 10");
        map.put("Kossowski", "Mateusz Kossowski = 24");
        map.put("Rębajło", "Renata Rębajło - 66");
        map.put("Antal", "Patrycja Antal - 34");
        map.put("Zalewski", "Maciej Zalewski - 45");
        map.put("Dębski", "Marcin Dębski - 50");

        Iterator<Map.Entry<String, String>> mapIterator = map.entrySet().iterator();
        while (mapIterator.hasNext()) {
            Map.Entry<String, String> entry = mapIterator.next();
            System.out.println(entry);
        }

        Iterator<String> valuesIterator = map.values().iterator();
        while (valuesIterator.hasNext()) {
            System.out.println(valuesIterator.next());
        }
    }
}

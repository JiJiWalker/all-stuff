package Lesson36;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Jakub on 10.04.2017.
 */
public class TreeSetIterator {
    public static void main(String[] args) {
        Set<Integer> numbers = new TreeSet<>();
        numbers.add(10);
        numbers.add(20);
        numbers.add(30);
        numbers.add(33);
        numbers.add(44);
        numbers.add(55);
        numbers.add(60);
        numbers.add(70);

       Iterator<Integer> numIterator = numbers.iterator();
       while (numIterator.hasNext()) {
           int number = numIterator.next();
           System.out.println(number);
       }
    }
}

package Lesson36;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Jakub on 10.04.2017.
 */
public class IteratorRemoveException {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Jeden");
        names.add("Dwa");
        names.add("Trzy");
        names.add("Cztery");
        names.add("Pięć");
        names.add("Sześć");
        names.add("Siedem");

        System.out.println(">>First loop<<");
        for (int i=0; i<names.size(); i++) {
            String name = names.get(i);
            System.out.println(name);
            if (name.equals("Pięć")) {
                names.remove(name);
            }
        }

        Iterator<String> nameIterator = names.iterator();
        System.out.println("\n"+">>Second loop<<");
        while (nameIterator.hasNext()) {
            String name = nameIterator.next();
            System.out.println(name);
            if (name.equals("Dwa")) {
                names.remove(name);
            }
        }

        System.out.println("\n"+">>Third loop<<");
        for (String name: names) {
            System.out.println(name);
            if (name.equals("Pięć")) {
                names.remove(name);
            }
        }
    }
}

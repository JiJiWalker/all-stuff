package Lesson36;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Jakub on 20.04.2017.
 */
public class IteratorRemoveWorking {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Basia");
        names.add("Zosia");
        names.add("Kasia");
        names.add("Asia");
        names.add("Beata");
        names.add("Kamila");

        Iterator<String> namesIterator = names.iterator();
        while (namesIterator.hasNext()) {
            String name = namesIterator.next();
            System.out.println(name);
            if (name.equals("Kasia")) {
                namesIterator.remove();
            }
        }
    }
}

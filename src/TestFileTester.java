import java.io.File;
import java.io.IOException;

/**
 * Created by Jakub on 01.03.2017.
 */
public class TestFileTester {
    public static void main(String[] args) {
        String fileName = "testFile.txt";
        File file = new File(fileName);

        boolean fileExist = file.exists();
        if (!fileExist) {
            try {
                fileExist = file.createNewFile();
            } catch (IOException e) {
                System.out.println("Can't make new file.");
            }
        }

        if (fileExist)
            System.out.println("File "+fileName+" exist or was just created.");
    }
}

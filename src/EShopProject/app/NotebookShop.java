package EShopProject.app;

import EShopProject.data.Computer;
import EShopProject.data.DataStore;

import javax.swing.*;

/**
 * Created by jakub on 08.12.16.
 */
public class NotebookShop {
    public static void main(String[] args){

        Computer computer = new Computer("Samsung", 1000);
        Computer computer1 = new Computer("Dell", 9999);
        Computer computer2 = new Computer("Samsung", 1000);
        Computer computer3 = new Computer("Sony", 1234);
        DataStore dataStore = new DataStore();

        dataStore.addComputer2(computer);
        dataStore.addComputer2(computer1);
        dataStore.addComputer2(computer2);
        dataStore.addComputer2(computer3);
        dataStore.printInfo();

        int computerFound = dataStore.checkAvaiability2(computer);
        System.out.println("Liczba takich samych komputerów: "+computerFound);

        System.out.println("Wszystkie dostępne komputery: ");
        for (Computer c: dataStore.getComputers()) {
            System.out.println(c);
        }
        //dataStore.chechAvalability(computer3);
        //dataStore.getComputers();
        //dataStore.checkAvaiability(computer);
        //JOptionPane.showInputDialog("Witaj świecie"); // wyświetla okno
        //dataStore.printInfo();
        //System.out.println(computer);
        //dataStore.addComputer2(computer);
        //System.out.println(computer);
        //dataStore.addComputer2(computer1);
        //dataStore.addComputer2(computer2);
        //dataStore.chechAvalability(computer1);
    }
}

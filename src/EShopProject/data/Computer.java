package EShopProject.data;

/**
 * Created by jakub on 08.12.16.
 */
public class Computer {

    private String producer;
    private int model;

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public Computer(String producer, int model) {
        setProducer(producer);
        setModel(model);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Computer)) return false;

        Computer computer = (Computer) o;

        if (getModel() != computer.getModel()) return false;
        return getProducer() != null ? getProducer().equals(computer.getProducer()) : computer.getProducer() == null;
    }

    @Override
    public int hashCode() {
        int result = getProducer() != null ? getProducer().hashCode() : 0;
        result = 31 * result + getModel();
        return result;
    }

    @Override
    public String toString() {
        return "Producent sprzętu: "+getProducer()
                +"\n"+"Model sprzętu: "+getModel();
    }
}

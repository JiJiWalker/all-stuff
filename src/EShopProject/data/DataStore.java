package EShopProject.data;

/**
 * Created by jakub on 08.12.16.
 */
public class DataStore {

    private static final int MAX_COMPUTERS_NUM = 100;
    private Computer[] computers;
    private int regComputer;

    public DataStore() {
        computers = new Computer[MAX_COMPUTERS_NUM];
        regComputer = 0;
    }

    /*public void addComputer(Computer computer) {
        if (regComputer < MAX_COMPUTERS_NUM){
            computers[regComputer] = computer;
            regComputer++;
        } else {
            System.out.println("Brak miejsca.");
        }
    }*/

    public void addComputer2(Computer computer) {
        try {
            computers[regComputer] = computer;
            regComputer++;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Brak miejsca.");
        }
    }

    public void printInfo() {
        for (int i = 0; i<regComputer; i++) {
            System.out.println("Producent: "+computers[i].getProducer()+". Model: "+computers[i].getModel());
        }
    }

    public void chechAvalability(Computer computer) {
        if (computer.equals(computers[regComputer])) {
            System.out.println("Jest coś podobnego.");
        } else {
            System.out.println("Nie ma czegoś podobnego.");
        }

    }

    public Computer[] getComputers() { ///////////////////////////////////
        Computer[] comps = new Computer[regComputer];
        for (int i = 0; i < regComputer; i++) {
            comps[i] = computers[i];
        }
        return comps;
    }

    public int checkAvaiability2(Computer find) { ////////////////////////// Ma sprawdzać czy są już podobne produkty w sklepie
        if (find == null)
            return 0;

        int count = 0;
        for (int i = 0; i < regComputer; i++) {
            if (find.equals(computers[i])) {
                count++;
            }
        }
        return count;
    }

}

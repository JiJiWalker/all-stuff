package Lesson40;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Jakub on 15.05.2017.
 */
public class StreamsGenerator {
    public static void main(String[] args) {
        Stream<Integer> numStream = Stream.iterate(0, x -> x+1);

        // na raz może być wywołana tylko jedna metoda .collect kiedy chcemy zrobić operację na liśćie.
        // tak samo jakbym chciał 2x zamknąć strumień (.close)
//        100 pierwszych liczb podzielnych przez 2
        List<Integer> numbers = numStream.filter(x -> x%2 == 0).limit(100).collect(Collectors.toList());
//        100 pierwszych kwadratów kolejnych 100 liczb całkowitych
        List<Integer> squareNumbers = numStream.map(x -> x*x).limit(100).collect(Collectors.toList());
//        100 liczb ujemnych w kolejności malejącej
        List<Integer> negativeNumbers = numStream.map(x -> -x).limit(100).collect(Collectors.toList());

        squareNumbers.forEach(System.out::println);
    }
}

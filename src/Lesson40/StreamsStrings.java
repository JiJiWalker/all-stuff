package Lesson40;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Jakub on 10.05.2017.
 */
public class StreamsStrings {
    public static void main(String[] args) {
        Stream<String> string = Stream.of("a", "bb", "ccc", "dddd", "eeeee", "ffffff", "ggggggg");
        List<String> stringList = string.map(String::toUpperCase)
                                        .peek(System.out::println)
                                        .collect(Collectors.toList());
        // List<String> stringList można byłoby pominąć
        // map. zmienia elementy strumienia w podaną fukcnję.

//        Stream.of("d1", "d2", "d3")
//                .filter(s -> {
//                    System.out.println("filter" + s);
//                return true; // musi być
//               })
//                .forEach(System.out::println);

    }
}

package Lesson40;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Jakub on 15.05.2017.
 */
public class Task {
    public static void main(String[] args) {
        List<Integer> specialNumbers = Stream.iterate(0, x -> x+1)
                                            .filter(x -> x > 100 && x < 1000 && x%5 == 0)
                                            .limit(10)
                                            .map(x -> x*3)
                                            .collect(Collectors.toList());
        specialNumbers.forEach(System.out::println);
    }
}

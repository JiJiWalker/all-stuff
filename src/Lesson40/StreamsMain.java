package Lesson40;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Jakub on 10.05.2017.
 */
public class StreamsMain {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        List<Integer> numbers2 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        numbers = numbers.stream()
                .filter(x -> x%2 == 0) // albo ...&& x>5);
                .filter(x -> x>5)
                .collect(Collectors.toList()); // Metoda collect() zwraca strumień w postaci kolekcji wybranego typu Collectora
        // .collect(Collectors.toCollection(ArrayList::new)); kolekcja konkretniejszego typu


        // Jeżeli chcemy zwrócić kolekcję konkretnego typu możemy posłużyć się ogólniejszą metodą toCollection() i przekazać jej jako argument referencję na konstruktor
        numbers2 = numbers2.stream()
                .filter(x -> x%2 == 0 && x>5)
                .collect(Collectors.toCollection(ArrayList::new));
        numbers.forEach(System.out::println);
        numbers2.forEach(System.out::println);

    }
}


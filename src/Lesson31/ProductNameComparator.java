package Lesson31;

import java.util.Comparator;

/**
 * Created by jakub on 19.03.17.
 */
public class ProductNameComparator implements Comparator<Product> {

    // można było zrobić tę klasę jako statyczną zagnieżdzoną w klasie Product
//    mimo wszystko idę rozumowaniem, żę nie powinno się robić klas w klasach, więc zrobiłem nową
    @Override
    public int compare(Product p1, Product p2) {
        return p1.getName().compareTo(p2.getName());
    }
}

package Lesson31.Task;

import java.util.Comparator;

/**
 * Created by jakub on 19.03.17.
 */
public class NumberComparatorClass implements Comparator<NumbersClass>{

    @Override
    public int compare(NumbersClass o1, NumbersClass o2) {
        return o1.getIntNumber().compareTo(o2.getIntNumber());
    }
}

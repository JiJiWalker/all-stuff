package Lesson31.Task;

/**
 * Created by jakub on 19.03.17.
 */
public class NumbersClass implements Comparable<NumbersClass> {

    Integer intNumber;

    public Integer getIntNumber() {
        return intNumber;
    }

    public void setIntNumber(Integer intNumber) {
        this.intNumber = intNumber;
    }

    public NumbersClass() {
    }

    public NumbersClass(Integer intNumber) {
        this.intNumber = intNumber;
    }

    @Override
    public String toString() {
        return "Twoja liczba: "+intNumber;
    }

    @Override
    public int compareTo(NumbersClass o) {
        return 0;
    }
}
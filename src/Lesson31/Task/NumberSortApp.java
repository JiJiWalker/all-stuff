package Lesson31.Task;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by jakub on 19.03.17.
 */
public class NumberSortApp {
    public static void main(String[] args) {
        Random rand = new Random();
        NumbersClass[] numClass = new NumbersClass[20];

        for (int i=0; i < numClass.length; i++) {
            numClass[i] = new NumbersClass(rand.nextInt(50));
            System.out.println(numClass[i]);
        }

        System.out.println("\nSortowanie w górę: ");
        Arrays.sort(numClass, new NumberComparatorClass());
        for (NumbersClass sort : numClass) {
            System.out.println(sort);
        }

        System.out.println("\nSortowanie w dół: ");
        Arrays.sort(numClass, new NumberComparatorClass().reversed());
        for (NumbersClass sort2 : numClass) {
            System.out.println(sort2);
        }

//        Poniżej jest klasa anonimowa, która utworzyła się w sumie sama po wpisaniu Array.sort i wypełnieniu
//        nawiasów. Implementujemy klasę Comparator z generycznym typem w nawiasach diamentowych. W przykładzie JavaStart jest to Integer
//        ale w klasie NumbersClass zainicjowaliśmy typ Integer + implementujemy tam Comparable. Nadpisana metoda
//        utworzyła się również już automatycznie. Zostało nam w return zamiast 0, porównać parametr o1 do o2,
//        i co najlepsze, żeby kolejność była odwrócona, dodajemy minus. Niestety nie działa ta opcja w tej klasie
//        Ze względu na implementowanie właśnie klasy NumbersClass a nie Integer. Gdybym usunął NumbersClass
//        I Bezpośrednio wywował klasę anonimową było by ok.
//
//        Arrays.sort(numClass, new Comparator<NumbersClass>() {
//            @Override
//            public int compare(NumbersClass o1, NumbersClass o2) {
//                return o1.compareTo(o2);
//            }
//        });
//
//        System.out.println("Sortowanie w dół z anonimową klasą: ");
//        System.out.println(Arrays.toString(numClass));
    }

}

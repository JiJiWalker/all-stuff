package Lesson31;

import java.util.Arrays;

/**
 * Created by jakub on 15.03.17.
 */
public class ProductCatalog {
    public static void main(String[] args) {
        Product[] products = new Product[7];
        products[0] = new Product("Samsung", "S6", "Phone");
        products[1] = new Product("Nokia", "Lumia 510", "Phone");
        products[2] = new Product("Panasonic", "AZ4100", "TV");
        products[3] = new Product("Amica", "C300", "Mini AGD");
        products[4] = new Product("Zelmer", "NeTe1", "AGD");
        products[5] = new Product("Samsung", "WX800", "TV");
        products[6] = new Product("Sony", "Vaio", "Laptop");

        System.out.println("Nieposortowane: ");
        for (Product unsort : products) {
            System.out.println(unsort);
        }

        System.out.println("\n"+"Posortowane: ");
        Arrays.sort(products);
        for (Product sort : products) {
            System.out.println(sort);
        }

        System.out.println("\n"+"Porostowane po nazwie");
        Arrays.sort(products, new ProductNameComparator());
        for (Product sort2 : products) {
            System.out.println(sort2);
        }


    }
}

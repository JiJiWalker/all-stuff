package Lesson31;

/**
 * Created by jakub on 15.03.17.
 */
public class Product implements Comparable<Product> {

    private String producer;
    private String name;
    private String category;

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Product(String producer, String name, String category) {
        this.producer = producer;
        this.name = name;
        this.category = category;
    }

    @Override
    public String toString() {
        return "Product [category = "+category+", producer = "+producer+", name = "+name+"]";
    }

//    porównaj z...
//    wygenerowane automatycznie bez typu generycznego z obiektem <Product>
//    bez tego obiektu mimo wszystko nie wyświetla podpowiedzi
//    @Override
//    public int compareTo(Object o) {
//        return 0;
//    }

//    możemy sortować w zależności od zmiennych (produkt/nazwa/categoria)
//    compareTo(arg) - zasady sortowania i zwracania wartości:
//      - wartość ujemna - jeśli (arg) ma być następnikiem obiektu
//      - wartość 0 - jeśli prównywane obiekty są równe
//      - wartość dodaną - jeśli obiekt oryginalny (this) jest większy ma być następnikiem (arg)
    @Override
    public int compareTo(Product o) {
        int categoryCompare = category.compareTo(o.getCategory());
        if (categoryCompare != 0) {
            return categoryCompare;
        }
        int producerCompare = producer.compareTo(o.getProducer());
        if (producerCompare != 0) {
            return producerCompare;
        }
        return name.compareTo(o.getName());
    }
}

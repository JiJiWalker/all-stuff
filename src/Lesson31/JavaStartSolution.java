package Lesson31;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

/**
 * Created by jakub on 21.03.17.
 */
public class JavaStartSolution {
    public static void main(String[] args) {
        Random random = new Random();
        Integer[] num = new Integer[20];

        for (int i=0; i<num.length; i++) {
            num[i] = random.nextInt(50);
        }

        System.out.println("Nieposortowane:");
        System.out.println(Arrays.toString(num));

        Arrays.sort(num);
        System.out.println("Posortowane w góre:");
        System.out.println(Arrays.toString(num));

//        Działa klasa anonimowa tutaj, która nie działała w klasie NumberSortApp
        Arrays.sort(num, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return -(o1.compareTo(o2));
            }
        });

        System.out.println("Posortowane w dół: ");
        System.out.println(Arrays.toString(num));
    }
}

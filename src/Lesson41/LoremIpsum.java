package Lesson41;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



/**
 * Created by Jakub on 26.05.2017.
 */
public class LoremIpsum {
    public static void main(String[] args) throws IOException {
        final String FILE_NAME = "Lorem.txt";

        BufferedReader br = new BufferedReader(new FileReader(FILE_NAME)); // wczytujemy plik
        List<String> words = new ArrayList<>(); // tworzymy nową listę
        String nextLine; //

        while ((nextLine = br.readLine()) != null) { // sprawdzamy czy kolejna linia jest pusta
            words.addAll(Arrays.asList(nextLine.split(" "))); // jeżeli nie jest dodajemy do listy słowo
        }

        words = words.stream() // zamieniamy words na stream
                    .map(s -> s.replaceAll(",", "").replaceAll("\\.", ""))
                    // aby wszystko pracowało na oryginalnej kolekcji używamy map i 2x replace
                    .collect(Collectors.toList());

        long countSWords = words.stream()
                                .filter(s -> s.startsWith("s")) // filtruje słowa zaczynające się na s
                                .peek(s -> System.out.print(s + " ")) // przechodzi przez strumień, ale nic nie robi poza wyłapaniem elemn. strumienia
                                .count(); // zwraca liczbę w postaci long reprezentującą ilośc elem. w strumieniu
        System.out.println();

        long fiveLetter = words.stream()
                                .filter(s -> s.length() == 5)
                                .peek(s -> System.out.print(s + " "))
                                .count();
        System.out.println();

        System.out.println("Liczba wyrazów na s: " + countSWords);
        System.out.println("Liczba wyrazów o długości 5 liter: " + fiveLetter);

        br.close();
    }
}

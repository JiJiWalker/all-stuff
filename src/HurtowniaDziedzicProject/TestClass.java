package HurtowniaDziedzicProject;

/**
 * Created by jakub on 04.12.16.
 */
public class TestClass extends Part {
    public static void main(String[] args) {
    
        Part part = new Part();
        part.model = "Cześć";
        part.series = 333;
        part.manufacturer = "Producent";
        part.idNr = 1;
        
        Tire tire = new Tire();
        tire.size = 12;
        tire.width = 5;
        tire.series = 444;
        
        Wheel wheel = new Wheel();
        wheel.size = 10;
        wheel.width = 3;
        
        ExhaustPart exPrt = new ExhaustPart();
        exPrt.iso = "Tak";

        System.out.println("Część " + part.model + "Seria " + part.series + "Producent " + part.manufacturer + "Identyfikator " + part.idNr);
        
    }
    
}

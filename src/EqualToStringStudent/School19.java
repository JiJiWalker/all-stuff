package EqualToStringStudent;

/**
 * Created by jakub on 08.12.16.
 */
public class School19 {
    public static void main(String[] args) {

        Student19 s1 = new Student19("Jan", "Pan");
        Student19 s2 = new Student19("Jan", "Pan");
        String studentDwa = s2.toString(); // przypisanie opisu obiektu do zmiennej

        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2)); // bez dodania metody equals w klasie student było tutaj false.

        System.out.println(s1.equals(s2));

        //s1.printInfo(); usunęliśmy to po dodaniu metody toString

        System.out.println(s1); // metoda toString wyświetla się sama.
        System.out.println(studentDwa);
    }
}

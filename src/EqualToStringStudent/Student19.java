package EqualToStringStudent;

/**
 * Created by jakub on 08.12.16.
 */
public class Student19 {

    String firstName;
    String lastName;

    public Student19(String fn, String ln) {
        this.firstName = fn;
        this.lastName = ln;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student19)) return false;

        Student19 student19 = (Student19) o;

        if (firstName != null ? !firstName.equals(student19.firstName) : student19.firstName != null) return false;
        return lastName != null ? lastName.equals(student19.lastName) : student19.lastName == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }
    //public void printInfo() {
      //  System.out.println("Imię "+firstName+" Nazwisko "+lastName);
    //}

    @Override
    public String toString() {
        return "Imię "+firstName+" Nazwisko "+lastName;
    }
}

/**
 * Created by jakub on 20.10.16.
 */
public class Zadanie13 {
    public static void main(String[] args) {

        double[][] tabAll = new double[3][3];
        tabAll[0][0] = 1.0;
        tabAll[0][1] = 1.5;
        tabAll[0][2] = 2.0;
        tabAll[1][0] = 1.5;
        tabAll[1][1] = 2.0;
        tabAll[1][2] = 2.5;
        tabAll[2][0] = 2.0;
        tabAll[2][1] = 2.5;
        tabAll[2][2] = 3.0;

        // można to zastąpić poniższym.

        double[][] tab = new double[3][];
        double[] line0 = {1.0, 1.5, 2.0};
        double[] line1 = {1.5, 2.0, 2.5};
        double[] line2 = {2.0, 2.5, 3.0};

        tab[0] = line0; // tworzę to niezależnie jako oddzielną tablicę.
        tab[1] = line1; // nie dodaję drugiej liczby w drugim nawiasie
        tab[2] = line2; // pod 0 1 i 2 są liczby z line0 line1 line2

        System.out.println(tab[1][2]);


        double sumIloczyn1 = tabAll[0][0]*tabAll[1][1]*tabAll[2][2];
        double sumIloczyn2 = tabAll[0][2]*tabAll[1][1]*tabAll[2][0];
        // double sumIloczynAll = sumIloczyn1+sumIloczyn2;
        System.out.println(sumIloczyn1+sumIloczyn2);

        double sumWiersz = tabAll[1][0]*tabAll[1][1]*tabAll[1][2];
        double sumaKolumna = tabAll[0][1]*tabAll[1][1]*tabAll[2][1];
        // double sumWierKol = sumWiersz+sumaKolumna;
        System.out.println(sumWiersz+sumaKolumna);

        double sumEdge = tabAll[0][0]+tabAll[0][1]+tabAll[0][2]+tabAll[1][0]+tabAll[1][2]+tabAll[2][0]+tabAll[2][1]+tabAll[2][2];
        System.out.println(sumEdge);
    }
}

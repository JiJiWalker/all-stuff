import java.util.Scanner;

/**
 * Created by jakub on 03.12.16.
 */
public class PrintNumbers16 {

    public static final int EXIT = 0;

    public static void printNumbers(int start, int end) {
        for (int i = start; i<=end; i++) {
            System.out.println(i+" ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int start, end;
        do {
            System.out.println("Pierwsza liczba: ");
            start = input.nextInt();
            System.out.println("Druga liczba: ");
            end = input.nextInt();
            printNumbers(start, end);

            System.out.println("Koniec programu - 0");
            System.out.println("Kontynuuj - 1");
        } while (input.nextInt() != EXIT);
            input.close();

    }
}

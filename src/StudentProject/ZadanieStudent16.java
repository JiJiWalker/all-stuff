package StudentProject;

/**
 * Created by jakub on 25.11.16.
 */
public class ZadanieStudent16 {

    public String firstName;
    public String lastName;
    public int indexNr;
    public static int studentNr = 0;


    static ZadanieStudent16[] students = new ZadanieStudent16[10];

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getIndexNr() {
        return indexNr;
    }

    public void setIndexNr(int indexNr) {
        this.indexNr = indexNr;
    }

    public static int getStudentNr() {
        return studentNr;
    }

    public static void setStudentNr(int studentNr) {
        ZadanieStudent16.studentNr = studentNr;
    }

    public static void printStudent() {
        System.out.println(studentNr);
    }

    public ZadanieStudent16() {

    }

    public ZadanieStudent16(String firstName, String lastName, int indexNr) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.indexNr = indexNr;
        studentNr++;
    }

    /*public static void addStudent(ZadanieStudent16 student) {
        students[studentNr] = student;
        studentNr++;
    }*/ // To jest niepotrzebna metoda gdy jest w konstruktorze studentNr++

}

public class RandomMath {
    public static void main(String[] args) {
        int outsideBrackets;
        double primalRandom, multiplyPrimalRandom, insideBrackets;

        for (int i = 0; i < 40; i++) {
            primalRandom = (Math.random());
            multiplyPrimalRandom = primalRandom*10;
            insideBrackets = multiplyPrimalRandom - 20;
            outsideBrackets = (int)multiplyPrimalRandom;
            System.out.println("Primal: " + primalRandom + " Convert (int): " + (int)primalRandom);
            System.out.println("MultPrimal: " + multiplyPrimalRandom + " Convert (int): " + (int)multiplyPrimalRandom);
            System.out.println("In Score: " + insideBrackets + " Convert (int): " + (int)insideBrackets);
            System.out.println("Out Score: " + multiplyPrimalRandom + " Convert (int): " + outsideBrackets + ", Minus 20: " + (outsideBrackets - 20) + "\n");
        }
    }
}

package Lesson28;

/**
 * Created by Jakub on 09.03.2017.
 */
public class BigInt {
    int num = 5;
    Integer number = new Integer(num);
    Integer number2 = Integer.valueOf(num);

    int num2 = 5;
    String number3 = Integer.toString(num2);

    String number4 = "5.5";
    double num4 = Double.parseDouble(number4);

    Integer numberX = 5; // autoboxing (opakowywanie)
    int otherNumberX = numberX; // unboxing (rozpakowywanie)
}

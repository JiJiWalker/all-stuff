package Lesson28;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by Jakub on 09.03.2017.
 */
public class BigNumbers {
    public static void main(String[] args) {
        double a = 0.7;
        double b = 0.3;
        System.out.println(a-b);

        BigDecimal aBig = BigDecimal.valueOf(a);
        BigDecimal bBig = BigDecimal.valueOf(b);
        System.out.println("aBig - bBig " + aBig.subtract(bBig));
        aBig = aBig.subtract(bBig); // przypisanie wyniku do zmiennej aBig


    }
    BigInteger big1 = new BigInteger("1290491294812848129");
    BigDecimal big2 = BigDecimal.valueOf(0.7);

//    obliczenia na liczbach
//    add(); dodawanie
//    subtract(); odejmowanie
//    multiply(); mnożenie
//    divide(); dzielenie
}

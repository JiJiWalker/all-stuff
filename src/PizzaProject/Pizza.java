package PizzaProject;

/**
 * Created by jakub on 27.12.16.
 */
public enum Pizza {
    MARGHERITA("Sos pomidorowy", "Ser"), CAPRICIOSA("Sos pomidorowy", "Ser", "Pieczarki"), PROSCIUTTO("Sos pomidorowy", "Ser", "Szynka");

    String ingredient1;
    String ingredient2;
    String ingredient3;

    Pizza(String ingredient1, String ingredient2) {
        this.ingredient1 = ingredient1;
        this.ingredient2 = ingredient2;
    }

    Pizza(String ingredient1, String ingredient2, String ingredient3) {
        this.ingredient1 = ingredient1;
        this.ingredient2 = ingredient2;
        this.ingredient3 = ingredient3;
    }

    public String getIngredient1() {
        return ingredient1;
    }

    public String getIngredient2() {
        return ingredient2;
    }

    public String getIngredient3() {
        return ingredient3;
    }
}

package PizzaProject;


import java.util.Random;
import java.util.Scanner;

/**
 * Created by jakub on 27.12.16.
 */
public class Pizzeria {
    public static void main(String[] args) {

        Pizza pizza;
        
        System.out.println("Witamy."+"\n");
        
        for (Pizza pe: Pizza.values()) { // zwraca null
            System.out.println("Dziś do wyboru: " + pe + " Składniki: " + pe.getIngredient1() + " " + pe.getIngredient2() + " " + pe.getIngredient3());
        }

        System.out.println("\n"+"Wybierz którą chcesz zamówić: ");
        System.out.println("Margherita.");
        System.out.println("Capriciosa.");
        System.out.println("Prosciutto."+"\n");

        Scanner sc = new Scanner(System.in);
        pizza = Pizza.valueOf(sc.nextLine());

        switch (pizza) {
            case MARGHERITA:
                System.out.println("\n"+"Wybrałeś pizzę Margherita");
                break;
            case CAPRICIOSA:
                System.out.println("\n"+"Wybrałeś pizzę Capriciosa.");
                break;
            case PROSCIUTTO:
                System.out.println("\n"+"Wybrałeś pizzę Prosciutto.");
                break;
            default:
                System.out.println("Zrobiłeś coś źle.");
        }
        sc.close();
        int time = new Random().nextInt(50);

        System.out.println("Dziękujemy. Wybrałeś "+pizza.name()+". Dojedzie ona w ciągu "+time+" minut.");

    }
}

package Lesson33;

import java.util.TreeSet;

/**
 * Created by jakub on 25.03.17.
 */
public class SetsExamples {
    public static void main(String[] args) {
        TreeSet<Integer> set = new TreeSet<>();

        set.add(1);
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);

        System.out.println("Size: "+set.size());
        System.out.println("First: "+set.first());
        System.out.println("Last: "+set.last());
        System.out.println("Contains 2: "+set.contains(2));
    }
}

package Lesson33;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Jakub on 26.03.2017.
 */
public class Task33FileReader {
    public static void main(String[] args) throws FileNotFoundException {
        String filename = "namespl.txt";
        File file = new File(filename);
        Scanner sc = new Scanner(file);

        int lines = 0;
        while (sc.hasNextLine()) {
            String name = sc.nextLine();
            System.out.println(name);
            lines++;
        }
        System.out.println("Ilość wierszy w pliku: "+lines);
        sc.close();
    }
}

package Lesson33;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Jakub on 26.03.2017.
 */
public class Task33BufferedReader {
    public static void main(String[] args) {
        String filename = "namespl.txt";
        try (FileReader fileReader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileReader)
            ) {
            String nextLine = null;
            int lines = 0;
            while ((nextLine = bufferedReader.readLine()) != null) {
                System.out.println(nextLine);
                lines++;
            }
            System.out.println("Ilość wierszy w pliku: "+lines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package Lesson33;

import java.util.HashSet;

/**
 * Created by jakub on 25.03.17.
 */
public class PersonSet {
    public static void main(String[] args) {
        HashSet<Person> person = new HashSet<>();

        person.add(new Person("Kasia", "Kasiewska"));
        person.add(new Person("Zosia", "Zosiewska"));
        person.add(new Person("Brat", "Bratowy"));
        person.add(new Person("Greg", "Gregski"));
        person.add(new Person("Roman", "Romanow"));

        System.out.println("Person size: "+person.size());
        System.out.println("Person contains Roman Romanow? "+person.contains(new Person("Roman", "Romanow")));
        person.remove(new Person("Greg", "Gregski"));
        System.out.println("Person new size: "+person.size());
    }
}

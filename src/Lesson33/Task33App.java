package Lesson33;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * Created by Jakub on 26.03.2017.
 */
public class Task33App {
    public static void main(String[] args) {
        String filename = "namespl.txt";
        TreeSet<String> set = new TreeSet<>(new ReverseOrder());

        try (FileReader fileReader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileReader)
            ) {
            String nextLine;
            int lines = 0;
            while ((nextLine = bufferedReader.readLine()) != null) {
                set.add(nextLine);
                lines++;
            }
            System.out.println("Ilośc wierszy w pliku: "+lines);
            System.out.println("Ilość unikalnych wierszy: "+set.size());
            System.out.println("Pierwszy element: "+set.first());
            System.out.println("Ostatni element: "+set.last()+"\n");

            for (String el: set) {


//                Pętla ta w połączeniu z tą powyżej wylicza po kolei ile jest konkretnych imion.
//                for (int i=0; i<el.length(); i++) {
//                    System.out.println("Imię "+i+": "+el);
//                }

                System.out.println(el);
            }

        } catch (FileNotFoundException e) {
            System.err.println("FileNotFoundException error");
        } catch (IOException e) {
            System.err.println("IOEception erroe");
        }
    }

    static class ReverseOrder implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            return -(o1.compareTo(o2));
        }

    }

}

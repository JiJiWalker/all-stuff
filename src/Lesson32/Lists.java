package Lesson32;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jakub on 21.03.17.
 */
public class Lists {
    public static void main(String[] args) throws InterruptedException {
        List<Integer> ints = new ArrayList<>();

        for (int i=0; i<100; i++) {
            ints.add(i);
        }

        System.out.println("Wielkość tablicy: "+ints.size());
        System.out.println("Ints.get(50): "+ints.get(50));
        System.out.println("Czyszczę indeks 50...");
        ints.remove(50);
        Thread.sleep(1000);
        System.out.println("Wyczyszczony.");
        System.out.println("Ints.get(49): "+ints.get(49));
        System.out.println("Nowy Ints.get(50): "+ints.get(50)); //liczby po wyczyszczeniu przesunęły się o jedno miejsce do tyłu.
        System.out.println("Czyszczę tablicę...");
        ints.clear();
        Thread.sleep(1000);
        System.out.println("Nowa wielkość tablicy: "+ints.size());
    }


}

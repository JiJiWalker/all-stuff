package CalculatorTryCatchProject;

import java.util.Scanner;

/**
 * Created by Jakub on 20.02.2017.
 */
public class CalculatorLogic {

    final String PLUS = "+";
    final String MINUS = "-";
    final String MULTIPLY = "*";
    final String DIVIDE = "/";

    public float calculated(float a, float b, String operator) throws UknowOperatorExeption {
        float result = 0;

        switch (operator) {
            case PLUS:
                result = a+b;
                break;
            case MINUS:
                result = a-b;
                break;
            case MULTIPLY:
                result = a*b;
                break;
            case DIVIDE:
                result = a/b;
                break;
            default:
                throw new UknowOperatorExeption("Unexpected command. Type operator...");
        }
        return result;
    }
}

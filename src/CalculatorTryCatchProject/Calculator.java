package CalculatorTryCatchProject;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Jakub on 19.02.2017.
 */
public class Calculator {
    public static void main(String[] args) throws UknowOperatorExeption {

        final String PLUS = "+";
        final String MINUS = "-";
        final String MULTIPLY = "*";
        final String DIVIDE = "/";
        //float result = 0;

        float a = 0;
        float b = 0;
        String operation;
        Scanner sc = new Scanner(System.in);
        boolean readComplete = false;

        while (!readComplete) {
            try {
                System.out.println("First number:");
                a = sc.nextFloat();
                sc.nextLine();
                System.out.println("Second number:");
                b = sc.nextFloat();
                sc.nextLine();
                readComplete = true;
            } catch (InputMismatchException e) {
                System.out.println("This is not a number.");
                sc.nextLine();
            }
        }

        System.out.println("What you want to do (+, -, *, /)?");
        boolean readOperation = false;

        while (!readOperation) {
            operation = sc.nextLine();
            try {
                switch (operation) {
                    case PLUS:
                        System.out.println(a + b);
                        readOperation = true;
                        break;
                    case MINUS:
                        System.out.println(a - b);
                        readOperation = true;
                        break;
                    case MULTIPLY:
                        System.out.println(a * b);
                        readOperation = true;
                        break;
                    case DIVIDE:
                        System.out.println(a / b);
                        readOperation = true;
                        break;
                    default:
                        throw new UknowOperatorExeption("Unexpected command.");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                //System.out.println("Wrong operator. What do you want to do?");
            }
        }
        sc.close();
    }
}

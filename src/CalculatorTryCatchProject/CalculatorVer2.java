package CalculatorTryCatchProject;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Jakub on 20.02.2017.
 */
public class CalculatorVer2 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        float a = 0;
        float b = 0;
        String operator = null;


        boolean readComplete = false;
        while (!readComplete) {
            try {
                System.out.println("First number.");
                a = sc.nextFloat();
                sc.nextLine();
                System.out.println("Operator");
                operator = sc.nextLine();
                System.out.println("Second number.");
                b = sc.nextFloat();
                sc.nextLine();
                readComplete = true;
            } catch (InputMismatchException e) {
                System.out.println("This is not a number.");
                sc.nextLine();
            }
        }

//        System.out.println("Operator");
//        operator = sc.nextLine();

        CalculatorLogic calc = new CalculatorLogic();
        float result = 0;
        boolean calculationComplete = false;

        while (!calculationComplete) {
            try {
                result = calc.calculated(a, b, operator);
                System.out.println(a+" "+operator+" "+b+" "+"="+" "+result);
                calculationComplete = true;
            } catch (UknowOperatorExeption e) {
                System.err.println(e.getMessage());
                operator = sc.nextLine();
            }
        }

//        System.out.println("Do you wanna try again?");
//        String answer;
//
//        try {
//            System.out.println("Yes");
//            answer = sc.nextLine();                                           // nie działa
//            System.out.println("No");
//            answer = sc.nextLine();
//        } catch (InputMismatchException e) {
//            System.out.println("This is not the answer. Yes or No?");
//        }

//        if (calculationComplete) {
//            System.out.println(a+" "+operator+" "+b+" "+"="+" "+result);
//        } else {
//            System.out.println("Cant solve this problem: "+a+" "+operator+" "+b);
//        }
        sc.close();
    }
}

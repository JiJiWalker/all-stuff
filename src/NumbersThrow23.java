/**
 * Created by Jakub on 18.02.2017.
 */
public class NumbersThrow23 {
    private int[] numbers;

    public NumbersThrow23() {
        numbers = new int[10];
    }

    public void add(int index, int number) throws ArrayIndexOutOfBoundsException {
//        if (index < 0 || index >= numbers.length) {
//            throw new ArrayIndexOutOfBoundsException(index);
//        }
//        numbers[index] = number;

        //może być też tak bardziej szczegółowo
        if (index < 0) {
            throw new ArrayIndexOutOfBoundsException("Index musi być większy od 0");
        } else if (index >= numbers.length) {
            throw new ArrayIndexOutOfBoundsException("Indeks musi być mniejszy od rozmiaru tablicy "+numbers.length);
        }
        numbers[index] = number;

        //index w tym kodzie do numer tablicy ([1], [3]...
        // a number to liczba przypisana do tej tablicy.
        // czyli numbers[indeks] = number;
        // to to samo co num.add(1, 3)
    }

    public int get(int index) throws ArrayIndexOutOfBoundsException {
        if (index < 0 || index >= numbers.length) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        return numbers[index];
    }
}

package Lesson29;

/**
 * Created by Jakub on 10.03.2017.
 */
public class Contener<T> {
    private T[] array;

    public T[] getArray() {
        return array;
    }

    public void setArray(T[] array) {
        this.array = array;
    }

    public T get(int index) {
        return array[index];
    }

    public void printObject() {
        for (T o: array) {
            System.out.println(o);
        }
    }
}

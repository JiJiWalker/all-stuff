package Lesson29;

/**
 * Created by Jakub on 10.03.2017.
 */
public class Pair<T, V> {
    private T t;
    private V v;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public V getV() {
        return v;
    }

    public void setV(V v) {
        this.v = v;
    }

    // Byłoby przydatne gdyby nie było konstruktora
//    public void add(T t, V v) {
//        this.t = t;
//        this.v = v;
//    }

    Pair(T t, V v) {
        this.t = t;
        this.v = v;
    }

    @Override
    public String toString() {
        return t + " " + v;
    }
}

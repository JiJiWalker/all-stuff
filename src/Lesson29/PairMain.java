package Lesson29;

/**
 * Created by Jakub on 10.03.2017.
 */
public class PairMain {
    public static void main(String[] args) {
        PairMain pairMain = new PairMain();
        Pair<Integer, String> firstPair = new Pair<>(10, "Babcia");
        Pair<Double, String> secondPair = new Pair<>(5.5, "Dziadek");
        Pair<Integer, String> thirdPair = new Pair<>(10201, "Małgosia");

        pairMain.printInfo(firstPair);
        pairMain.printInfo(secondPair);
        pairMain.printInfo(thirdPair);

    }

    public <T, V> void printInfo(Pair<T, V> pair) {
        System.out.println(pair);
    }
}

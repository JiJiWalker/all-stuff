package Lesson29;

/**
 * Created by Jakub on 10.03.2017.
 */
public class GenericTester {
    public static void main(String[] args) {
//        Definiujemy konterer przechowujący liczby całkowite
        Contener<Integer> integers = new Contener<Integer>();
//        Przypisujemy nową tablicę typu Integer
        integers.setArray(new Integer[5]);
//        Do pierwszego elementu przypisujemy liczbę 5
        integers.getArray()[0] = 5;

//        Tworzymy kontener przechowujący obiekty String
        Contener<String> strings = new Contener<String>();
//        Przypisujemy tablicę typu String
        strings.setArray(new String[10]);
//        Przypisujemy 1 i 2 element tablicy
        strings.getArray()[0] = "Leonardo";
        strings.getArray()[1] = "Rafaello";

//        Odczytywanie danych bez konieczności rzutowania
        Integer num = integers.get(0);
        String str = strings.get(0);
        String str2 = strings.get(1);

//        Wyświetlenie wartości
        System.out.println(num);
        System.out.println(str);
        System.out.println(str2);
    }
}

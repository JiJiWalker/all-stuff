package WordProject.app;

import WordProject.data.Human16;

/**
 * Created by jakub on 03.12.16.
 */
public class World16 {
    public static void main(String[] args) {

        Human16 adam = new Human16("Adam", (float) 186.6);
        Human16 eve = new Human16("Ewa", (float) 159.0);

        float avgHeight = (adam.getHeight() + eve.getHeight() / 2);
        Human16.avgHeight = avgHeight;

        System.out.println("Pierwsi ludzie na ziemi: "+"\n");
        System.out.println(adam.getName()+" "+adam.getHeight()+"cm.");
        System.out.println(eve.getName()+" "+eve.getHeight()+"cm.");

        System.out.println("\n"+"Średni wzrost: ");
        System.out.println("Adam: "+adam.getHeight());
        System.out.println("Eve: "+eve.getHeight());
        System.out.println("Na ziemi: "+Human16.avgHeight);
    }
}

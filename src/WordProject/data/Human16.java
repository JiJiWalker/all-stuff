package WordProject.data;

/**
 * Created by jakub on 03.12.16.
 */
public class Human16 {

    private String name;
    private float height;
    public static float avgHeight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public Human16(String name, float height) {
        setName(name);
        setHeight(height);
    }
}

package Lesson42;

import java.time.LocalTime;

/**
 * Created by Jakub on 06.07.2017.
 */
public class LocalTimeTest {
    public static void main(String[] args) throws InterruptedException {
        LocalTime lc1 = LocalTime.of(12, 34, 59);
        System.out.println(lc1);
        LocalTime lc2 = LocalTime.now();

        for (int i=0; i<10; i++) {
            System.out.println("Aktualny czas: "+lc2);
            lc2 = lc2.plusSeconds(1);
            Thread.sleep(1000);
        }
    }
}

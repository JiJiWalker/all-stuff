package Lesson42;

import java.time.LocalDate;

/**
 * Created by Jakub on 06.07.2017.
 */
public class LocalDateTest {
    public static void main(String[] args) {
        LocalDate lcNow = LocalDate.now();
        System.out.println(lcNow);

        LocalDate sixOct2017 = LocalDate.of(2017, 10, 6);
        System.out.println( sixOct2017);

        System.out.println(lcNow.getDayOfYear());
    }
}

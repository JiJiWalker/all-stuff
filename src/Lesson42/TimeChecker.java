package Lesson42;

import java.time.LocalDate;

/**
 * Created by Jakub on 06.07.2017.
 */
public class TimeChecker {
    public static void main(String[] args) {
        LocalDate now = LocalDate.now();
        LocalDate firstJan2014 = LocalDate.of(2014, 1, 1);

        boolean check = now.isAfter(firstJan2014);
        System.out.println("Czy "+now+" jest po "+firstJan2014);
        System.out.println(check);
    }
}

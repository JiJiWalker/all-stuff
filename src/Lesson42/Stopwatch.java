package Lesson42;

import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

/**
 * Created by Jakub on 04.06.2017.
 */
public class Stopwatch {
    public static void main(String[] args) {

       try (Scanner sc = new Scanner(System.in)) {
           System.out.println("To start timer insert ENTER");
           sc.nextLine();
           Instant startTime = Instant.now();

           System.out.println("To end timer insert ENTER");
           sc.nextLine();
           Instant endTime = Instant.now();

           Duration stopwatch = Duration.between(startTime, endTime);
           System.out.println("Gap beetwen time is "+stopwatch.getSeconds());
       }
    }
}

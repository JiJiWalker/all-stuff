package Lesson42;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;

/**
 * Created by Jakub on 01.06.2017.
 */
public class TimeRecorded {
    public static void main(String[] args) throws InterruptedException {

        LocalTime now = LocalTime.now();
//
//        for (int i = 0; i<10; i++) {
//            System.out.println(now);
//            now = now.plusSeconds(1);
//            Thread.sleep(1000);
//        }

        Instant actualTime = Instant.now();
        //System.out.println(actualTime);

        Instant sec = Instant.ofEpochSecond(200000);
        //System.out.println(sec);

        for (int i=0; i<100; i++) {
            System.out.println("Miernik czasu");
        }
        Instant actualTime2 = Instant.now();
        Duration gap = Duration.between(actualTime, actualTime2);
        System.out.println("Upłynęło "+gap.getNano()+" sekund");
    }
}

package Lesson42;

import java.time.Instant;
import java.time.LocalTime;
import java.util.Scanner;

/**
 * Created by Jakub on 06.07.2017.
 */
public class StopWatchV2 {
    public static void main(String[] args) {

        // Appka do skończenia. Nie wiem jak zrobić flagę, aby końćzyło mi po 2 enter.
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("To start press ENTER");
            System.out.println("To stop press ENTER second time.");
            boolean isEnd = true;
            LocalTime startLocalTime = LocalTime.MIN;
            String flag;
            flag = sc.nextLine();

            if (flag.equals("a")) {
                isEnd = false;
            }

            do {
                startLocalTime = startLocalTime.plusSeconds(1);
                System.out.println(startLocalTime);
                Thread.sleep(1000);
            } while (isEnd);

            System.out.println("dupa");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

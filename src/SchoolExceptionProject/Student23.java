package SchoolExceptionProject;

/**
 * Created by Jakub on 18.02.2017.
 */
public class Student23 {
    private int studentId;
    private String firstName;
    private String lastName;

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Student23(int studentId, String firstName, String lastName) {
        this.studentId = studentId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Student - " + "ID: " + studentId +
                ", Imię: " + firstName +
                ", Nazwisko: " + lastName;
    }
}

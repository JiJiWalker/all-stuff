package SchoolExceptionProject;

/**
 * Created by Jakub on 19.02.2017.
 */
public class SchoolTest23 {
    public static void main(String[] args) {

        School23 school = new School23(3);
        try {
            school.add(new Student23(1, "Jan", "Kowalski"));
            school.add(new Student23(2, "Marek", "Bobowski"));
            school.add(new Student23(3, "Sylwek", "Przanowski"));
            school.add(new Student23(4, "Bartek", "Kowalski"));
        } catch (NoMoreSpaceExeption23 e) {
            System.err.println("Nie można dodać tylu osób do szkoły.");
            System.err.println(e.getMessage());
            //e.printStackTrace();
        }

        try {
            System.out.println(school.find("Marek", "Bobowski"));
            System.out.println(school.find("Sylwek", "Przanowski"));
            System.out.println(school.find("Darek", "Knyziak"));
        } catch (NoElementFoundException23 e) {
            System.err.println("Nie znaleziono takiej osoby.");
            System.err.println(e.getMessage());
            //e.printStackTrace();
        }
    }
}

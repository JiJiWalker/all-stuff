package SchoolExceptionProject;

/**
 * Created by Jakub on 18.02.2017.
 */
public class School23 {
    private Student23[] students;
    private int studentNumber;

    public School23(int studentNumber) {
        students = new Student23[studentNumber];
        this.studentNumber = 0;
    }

    public void add(Student23 s) throws NoMoreSpaceExeption23 {
        if (studentNumber >= students.length) {
            throw new NoMoreSpaceExeption23("Brak miejsca w tablicy School. Ilość miejsc:  "+students.length);
        } else {
            students[studentNumber] = s;
            studentNumber++;
        }
    }

    public Student23 find(String firsName, String lastName) throws NoElementFoundException23 {
        boolean found = false;
        Student23 foundElement = null;
        int index = 0;
        while (!found && index < students.length) {
            if (students[index].getFirstName().equals(firsName) && students[index].getLastName().equals(lastName)) {
                foundElement = students[index];
                found = true;
            } else {
                index++;
            }
        }

        if (foundElement == null) {
            throw new NoElementFoundException23("Nie znaleziono elementu: "+firsName+" "+lastName);
        }
        return foundElement;
    }
}

import java.io.*;

/**
 * Created by Jakub on 01.03.2017.
 */
public class TestFileTester2 {
    public static void main(String[] args) {
        String fileName = "testFile.txt";
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileName));
            String tmp = null;
            while ((tmp = reader.readLine()) != null) {
                System.out.println(tmp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            writer.write("Bartek");
            writer.newLine();
            writer.write("Wojtek");
            writer.newLine();
            writer.write(3);
        } catch (IOException e) {
            System.err.println("Write error!");
        }
    }
}

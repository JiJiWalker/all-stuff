/**
 * Created by jakub on 20.10.16.
 */
public class MuldiDimensional13 {
    public static void main(String[] args) {

        int[] longTab = new int[1000];
        longTab[99] = 100;
        int[] midTab = new int[100];
        int[] smallTab = new int[10];

        int[][] hugeTab = new int[3][];
        hugeTab[0] = longTab;
        hugeTab[1] = midTab;
        hugeTab[2] = smallTab;

        System.out.println("Długoś tablic: ");
        System.out.println(hugeTab[0].length+" "+hugeTab[1].length+" "+hugeTab[2].length);
        System.out.println(hugeTab[0][99]);
        
        hugeTab[1][1] = 123;
        System.out.println(hugeTab[1][1]);
    }
}

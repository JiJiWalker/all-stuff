package SuperFruit;

/**
 * Created by jakub on 05.12.16.
 */
public class Apple extends Fruit {

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Apple(String type) {
        this.type = type;
    }

    @Override
    public void printInfo() {
        super.printInfo();
        //printApple(type);
        System.out.println("Jabłko "+type);
    }

    //public void printApple(String type) {
      //  System.out.print(type); // to mogłoby być niepotrzebne, ale działa.
    //}

}

package KursJava.exceptions;

import KursJava.controller.Zadanie10PointController;
import KursJava.data.Zadanie10Point;

/**
 * Created by jakub on 31.10.16.
 */
public class Dodatek10PointError {
    Zadanie10PointController p = new Zadanie10PointController();

    public void BrakDanychException()  {
        try {
            p.addX(new Zadanie10Point());
        } catch (BrakDanychException e) {

        } catch (NullPointerException ignored) {

        }
    }

    public void ZleDaneException() {
        try {
            p.minusX(new Zadanie10Point());
        } catch (ZleDaneException zleDane){

        } catch (BrakDanychException brakDanych) {

        }
    }
}

package KursJava.exceptions;


/**
 * Created by jakub on 31.10.16.
 */
public class ZleDaneException extends Exception{
    private static String message = "Złe dane";

    public ZleDaneException() {
        super(message);
    }
}

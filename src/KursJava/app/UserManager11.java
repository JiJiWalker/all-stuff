package KursJava.app;

import KursJava.data.User11;

import java.util.Scanner;

/**
 * Created by jakub on 09.10.16.
 */
public class UserManager11 {
    public static void main(String[] args) {

        User11 user = new User11("Jan", "Kowalski");

        System.out.println("Wybierz czynność: "+"\n"
                +"0 - wyjście z programu."+"\n"
                +"1 - informacje o użytkowniku."+"\n"
                +"2 - modyfikuj użytkownika.");

        Scanner input = new Scanner(System.in);
        String opcja;
        String opcja2;

        Scanner input2 = new Scanner(System.in);
    // musi być pod innym input bo Java wywołuje to jako odpowiedź na "Imię" i dodaje jako 'opcja'
        int option;
        option = input2.nextInt();


        if(option == 0) {
            System.out.println("Bye bye!");
        } else if(option == 1) {
            System.out.println("Użytkownik: "+user.getFirstname()+" "+user.getLastname());
        } else if(option == 2) {
            System.out.println("Imię:");
            opcja = input.nextLine();
            user.setFirstname(opcja);
            System.out.println("Nazwisko: ");
            opcja2 = input.nextLine();
            user.setLastname(opcja2);
            System.out.println("Zmieniono dane użytkownika na: "+user.getFirstname()+" "+user.getLastname());
        }
    }
}

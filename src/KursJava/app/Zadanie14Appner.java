package KursJava.app;

import KursJava.data.Zadanie14Scanner;

import java.util.Locale;
import java.util.Scanner;

/**
 * Created by jakub on 22.10.16.
 */
public class Zadanie14Appner {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        input.useLocale(Locale.US);

        System.out.println("Podaj pierwszą liczbę.");
        double a = input.nextDouble();
        System.out.println("Podaj rodzaj działania (+, -, *, /).");
        input.nextLine();
        String operator = input.nextLine();
        System.out.println("Podaj drugą liczbę.");
        double b = input.nextDouble();

        Zadanie14Scanner calc = new Zadanie14Scanner();
        double result = calc.działanie(a, b, operator);
        System.out.println(a+" "+operator+" "+b+" "+"="+" "+result);

        input.close();
    }
}

package KursJava.app;

import KursJava.data.TargetEdge;

/**
 * Created by jakub on 23.10.16.
 */
public class TargetApp {
    public static void main(String[] args) {

        TargetEdge pkt = new TargetEdge(80000, 54600.5);
        pkt.calcPercent();
        pkt.addScore(2000);

        pkt.calcPercent();

        pkt.workWithp(3000);
    }
}

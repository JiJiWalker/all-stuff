package KursJava.app;

import KursJava.data.Zadanie12a;

/**
 * Created by jakub on 19.10.16.
 */
public class Zadanie12b {
    public static void main(String[] args) {

        Zadanie12a[] tablica1 = new Zadanie12a[3];
        tablica1[0] = new Zadanie12a(4);
        tablica1[1] = new Zadanie12a(10);
        tablica1[2] = new Zadanie12a(33);
        
        Zadanie12a[] tablica2 = new Zadanie12a[3];
        tablica2[0] = new Zadanie12a(1);
        tablica2[1] = new Zadanie12a(9);
        tablica2[2] = new Zadanie12a(3);

        // prostrza forma.
        int[] tablica3 = {1, 2, 4}; // forma wtedy kiedy wiemy jakie wartośći nmany przypisać.

        int sum3 = tablica3[0]+tablica3[1]+tablica3[2];
        System.out.println("Tablica 3 gdzie jest int[] i wartości w {} wynosi: "+sum3);
        
        int tablica1Size = tablica1.length;
        int tablica2Size = tablica2.length;

        System.out.println("Wielkość tablicy 1: "+tablica1Size);
        System.out.println("Wielkość tablicy 2: "+tablica2Size);
        System.out.println("Liczba w 1 indeksie 1 tablicy: "+tablica1[0].getLiczba());


        int suma1 = tablica1[0].getLiczba()+tablica1[1].getLiczba()+tablica1[2].getLiczba();
        System.out.println("Suma liczb w pierwszej tablicy: "+suma1);
        int suma2 = tablica2[0].getLiczba()+tablica2[1].getLiczba()+tablica2[2].getLiczba();
        System.out.println("Suma liczb w drugiej tablicy: "+suma2);
        int sumaAll = suma1+suma2;

        System.out.println("Syma wszystkich liczba: "+sumaAll);
    }
}

package KursJava.app;

import KursJava.controller.Zadanie10PointController;
import KursJava.data.Zadanie10Point;

/**
 * Created by jakub on 04.10.16.
 */
public class Zadanie10PointApplication {

    // akcja ta została dodana w lekcjio 11. Takie coś musi być przed main.
    public static final int ADD_X = 0;
    public static final int ADD_Y = 1;
    public static final int MINUS_X = 2;
    public static final int MINUS_Y = 3;

    public static void main(String[] args) throws Exception {

        Zadanie10Point pkt = new Zadanie10Point(5, 5);
        Zadanie10PointController p2 = new Zadanie10PointController();

        pkt.printInfo();

        p2.addX(pkt);
        pkt.printInfo();

        p2.addY(pkt);
        pkt.printInfo();

        p2.minusX(pkt);
        pkt.printInfo();

        p2.minusY(pkt);
        pkt.printInfo();

        // metoda ta nie zmienia przypisanych wartości. Jest ona zastępowana tym co już jest przypisane do x y
        p2.changeObject(pkt);
        pkt.printInfo();

        int option = ADD_X;


        switch (option) {
            case ADD_X:
                p2.addX(pkt);
                break;
            case ADD_Y:
                p2.addY(pkt);
                break;
            case MINUS_X:
                p2.minusX(pkt);
                break;
            case MINUS_Y:
                p2.minusY(pkt);
                break;
            default:
                System.out.println("Nieznana wartość.");
        }
        System.out.println("Punkt po zmianie: "+pkt.getX()+";"+pkt.getY());
    }
}

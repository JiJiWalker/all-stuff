package KursJava.data;

/**
 * Created by jakub on 13.10.16.
 */
public class Employee12 {

    private String firstName;
    private String lastName;
    private double salary;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Employee12() {

    }

    public Employee12(String firstName, String lastName, double salary) {
        setFirstName(firstName);
        setLastName(lastName);
        setSalary(salary);
    }

    public void prinInfo() {
        String info = "Imię: "+getFirstName()+" Nazwisko: "+getLastName()+" Hajs: "+getSalary();
        System.out.println(info);
    }
}

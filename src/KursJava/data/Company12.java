package KursJava.data;

/**
 * Created by jakub on 13.10.16.
 */
public class Company12 {
    public static void main(String[] args) {

        Employee12[] employees = new Employee12[20];
        employees[0] = new Employee12("Jacek", "Jackowicz", 2000.00);
        employees[1] = new Employee12("Marek", "Markiewicz", 2500.00);
        employees[2] = new Employee12("Jula", "Julkiewicz", 3000.00);
        employees[3] = new Employee12("Iza", "Izabelewicz", 4500.00);
        employees[4] = new Employee12("Amarena", "Wińska", 15000.00);

        int employeeIndex = 5;
        int employeeSize = employees.length;

        System.out.println(employees[employeeIndex-1].getFirstName()+" "+employees[employeeIndex-1].getLastName()+" "+employees[employeeIndex-1].getSalary()+" zł brutto.");
        System.out.println(employees[1].getFirstName()+" "+employees[1].getLastName()+" "+employees[1].getSalary()+" zł brutto.");
        System.out.println(employees[2].getFirstName()+" "+employees[2].getLastName()+" "+employees[2].getSalary()+" zł brutto.");
        //System.out.println(employees[20]); //nie działa bo nie ma takiego indexu jak 20. 
        System.out.println("Wielkość tablicy: "+employeeSize);
        System.out.println("Ostatni element tablicy to: "+employees[employeeSize-1]);



    }
}

package KursJava.data;

/**
 * Created by jakub on 22.10.16.
 */
public class Zadanie14Scanner {

    private String dod = "+";
    private String ode = "-";
    private String mno = "*";
    private String dziel = "/";

    public double działanie(double a, double b, String operator) {
        double result = 0;

        if (operator.equals(dod)) {
            result = a+b;
        }
        else if (operator.equals(ode)) {
            result = a-b;
        }
        else if (operator.equals(mno)) {
            result = a*b;
        }
        else if (operator.equals(dziel)) {
            result = a/b;
        }
        else {
            System.out.println("Zły operator");
        }

        return result;
    }

    /* może też być poniższy kod

    !! Zmienne takie jak wyżej albo !!
    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String MULTIPLY = "*";
    public static final String DIVIDE = "/";

    public double działanie(double a, double b, String operator) {
    result = 0

    switch (operator) {
    case PLUS:
        result = a+b;
        break;
    case MINUS:
        result = a-b;
        break;
    case MULTIPLY:
        result = a*b;
        break;
    case DIVIDE:
        result = a/b;
        break;
    default:
        sysout "zły kod"
    }

    return result;
    */
}

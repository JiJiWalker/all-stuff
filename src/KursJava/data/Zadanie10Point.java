package KursJava.data;

/**
 * Created by jakub on 04.10.16.
 */
public class Zadanie10Point {

    private Integer x;
    private int y;

    public Integer getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Zadanie10Point() {

    }

    public Zadanie10Point(Integer x) {
        this.setX(x);
    }

    public Zadanie10Point(int x, int y) {
        this.setX(x);
        this.setY(y);
    }

    public void printInfo() {
        String info = "Punkt x = "+getX()+"."+"\n"+"Punkt y = "+getY()+"."+"\n";
        System.out.println(info);
    }
}

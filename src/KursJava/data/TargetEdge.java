package KursJava.data;

/**
 * Created by jakub on 23.10.16.
 */
public class TargetEdge {

    int target;
    double score;
    double wwp = 0;

    public double getWwp() {
        return wwp;
    }

    public void setWwp(double wwp) {
        this.wwp = wwp;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public TargetEdge(int target, double score) {
        this.target = target;
        this.score = score;
    }

    public void calcPercent() {
        double result = (score *100) / target;
        System.out.println(result);
    }

    public void addScore(double dodaj) {
        setScore(getScore()+dodaj);
    }

    public void workWithp (double wwp) {
        setWwp(getWwp()+wwp);
    }

}

package KursJava.data;

/**
 * Created by jakub on 09.10.16.
 */
public class User11 {

    private String firstname;
    private String lastname;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public User11() {

    }

    public User11(String firstname, String lastname){
        this.firstname = firstname;
        this.lastname = lastname;
    }
}

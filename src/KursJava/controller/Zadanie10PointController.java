package KursJava.controller;

import KursJava.data.Zadanie10Point;
import KursJava.exceptions.BrakDanychException;
import KursJava.exceptions.Dodatek10PointError;
import KursJava.exceptions.ZleDaneException;

/**
 * Created by jakub on 04.10.16.
 */
public class Zadanie10PointController extends Dodatek10PointError{

    Zadanie10Point p;
    // stworzony został obiekt klasy p
    public void addX(Zadanie10Point p) throws BrakDanychException{
        if(null==p.getX()) throw new BrakDanychException();
        p.setX(p.getX()+1);
    }

    public void minusX(Zadanie10Point p) throws ZleDaneException, BrakDanychException{
        if (null==p.getX()) throw new BrakDanychException();
        if (p.getX()>10) throw new ZleDaneException();
        p.setX(p.getX()-1);
    }

    public void addY(Zadanie10Point p) {
        p.setY(p.getY()+1);
    }

    public void minusY(Zadanie10Point p) {
        p.setY(p.getY()-1);
    }

    public void changeObject(Zadanie10Point p) {
        p = new Zadanie10Point(-100, -100);
    }

}

package Lesson30;

import java.util.Arrays;

/**
 * Created by Jakub on 10.03.2017.
 */
public class ArrayExample {
    public static void main(String[] args) {
        String[] names = {"Basia", "Kasia", "Wojtek", "Marcela", "Ezio", "Maurycy"};
        Integer[] numbers = {9, 92, 10, 0, 6, 3, 997, 27};

        System.out.println("Names: ");
        printArray(names);

        System.out.println("NumbersClass: ");
        printArray(numbers);
        System.out.println();

        //sortowanie
        System.out.println("Arrays.sort(numbers): ");
        Arrays.sort(numbers);
        printArray(numbers);

        System.out.println("Arrays.sort(names)");
        Arrays.sort(names);
        printArray(names);
        System.out.println();

        // copyOf
        System.out.println("Numbers2, Array.copyOf()");
        Integer[] numbers2 = Arrays.copyOf(numbers, numbers.length);
        printArray(numbers2);
        System.out.println();

        //equals
        System.out.println("Arrays.equals(numbers, numbers2)");
        System.out.println(Arrays.equals(numbers, numbers2));
        System.out.println();

        //fill
        System.out.println("Arrays.fill()");
        String[] strings = new String[5];
        Arrays.fill(strings, "Wacek");
        printArray(strings);
    }

    public static <T> void printArray(T[] tArg) {
        for (T t : tArg) {
            System.out.print(t + " ");
        }
        System.out.println();
    }
}

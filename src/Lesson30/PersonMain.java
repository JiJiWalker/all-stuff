package Lesson30;

/**
 * Created by Jakub on 12.03.2017.
 */
public class PersonMain {
    public static void main(String[] args) {
        PersonDatabase pd = new PersonDatabase();
        pd.add(new Person("aa", "bb", "123"));
        pd.add(new Person("cc", "dd", "321"));
        pd.add(new Person("ee", "ff", "999"));

        System.out.println(pd);
    }
}

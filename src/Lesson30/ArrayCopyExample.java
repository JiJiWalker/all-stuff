package Lesson30;

/**
 * Created by Jakub on 10.03.2017.
 */
public class ArrayCopyExample {
    public static void main(String[] args) {
        int[] numbers1 = {1, 2, 3, 4, 5, 6, 7};
        int[] numbers2 = {10, 20, 30, 40, 50, 60, 70};

        //src - tablica żródłowa
        //srcPos - index od którego chcemy skopiować elementy
        //dest - tablicę do której kopiujemy
        //destPos - index od którego mają być wstawiane elementy
        //length - ilość elementów do skopiowania
        System.arraycopy(numbers1, 4, numbers2, 0, 3);

        for (int num : numbers2) {
            System.out.println(num + " ");
        }
    }
}

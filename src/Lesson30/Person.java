package Lesson30;

/**
 * Created by Jakub on 10.03.2017.
 */
public class Person {
    String firstName;
    String lastName;
    String pesel;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public Person(String firstName, String lastName, String pesel) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) return false;
        if (lastName != null ? !lastName.equals(person.lastName) : person.lastName != null) return false;
        return pesel != null ? pesel.equals(person.pesel) : person.pesel == null;
    }

    @Override
    public String toString() {
        return "Imię: "+firstName+"\n"
                +"Nazwisko: "+lastName+"\n"
                +"Pesel: "+pesel;
    }
}

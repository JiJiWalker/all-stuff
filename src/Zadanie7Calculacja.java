import java.util.Random;

/**
 * Created by jakub on 28.09.16.
 */
public class Zadanie7Calculacja {
    public static void main(String[] args) {

        double a = 15.15;
        double b = 42.3;

        Zadanie7Calculator action = new Zadanie7Calculator();

        action.add(a, b);
        action.subtract(a, b);
        action.multiply(a, b);
        action.divide(a, b);

        System.out.println(action.add(a, b));
        System.out.println(action.subtract(a, b));
        System.out.println(action.multiply(a, b));
        System.out.println(action.divide(a, b));

    }
}

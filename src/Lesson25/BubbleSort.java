package Lesson25;

/**
 * Created by Jakub on 26.02.2017.
 */
public class BubbleSort {
    public static void main(String[] args) {

        int bubbleArray[] = new int[] {5,2,1, 22, 13, 4, 26, 51,9, 100, 15};
        System.out.println("NumbersClass before sort.");
        for (int i=0; i<bubbleArray.length; i++) {
            System.out.print(bubbleArray[i] + " ");
        }

        bubbleSort(bubbleArray);

        System.out.println("");

        System.out.println("NumbersClass after sort.");
        for (int i=0; i<bubbleArray.length; i++) {
            System.out.print(bubbleArray[i] + " ");
        }

    }

    private static void bubbleSort(int[] bubbleArray) {
        int n = bubbleArray.length;
        int temp;

        for (int i=0; i<n; i++) {
            for (int j=1; j<n; j++) {

                if (bubbleArray[j-1] > bubbleArray[j]) {
                    temp = bubbleArray[j-1];
                    bubbleArray[j-1] = bubbleArray[j];
                    bubbleArray[j] = temp;
                }
            }
        }
    }
}

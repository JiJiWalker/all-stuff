package Lesson25;

/**
 * Created by Jakub on 24.02.2017.
 */
public class CarTest {
    public static void main(String[] args) throws InterruptedException {

        Car car = new Car();
        car.refuel(120);
        car.go();
    }
}

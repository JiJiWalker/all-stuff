package Lesson25;

/**
 * Created by Jakub on 25.02.2017.
 */
public interface BiggestNumber {
    int takeBiggestNumber(int[] tab);
}

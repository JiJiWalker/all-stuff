package Lesson25.BubbleSortVer2;

/**
 * Created by Jakub on 27.02.2017.
 */
public interface Sortable {
    public int[] sort(int[] tab);
}

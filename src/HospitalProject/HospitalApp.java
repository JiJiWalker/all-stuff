package HospitalProject;

import java.util.Scanner;

/**
 * Created by jakub on 19.11.16.
 */
public class HospitalApp {
    public static final int EXIT = 0;
    public static final int ADD_PATIENT = 1;
    public static final int PRINT_PATIENT = 2;
    public static final int CHECK_PATIENT = 3;


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int option = -1;

        Hospital hospital = new Hospital();

        while (option != EXIT){
            System.out.println("Dostęþne opcje:"+"\n");
            System.out.println("0 - wyjśćie z programu.");
            System.out.println("1 - dodanie pacjenta.");
            System.out.println("2 - lista pacjentów.");
            System.out.println("3 - ilość wolnych miejsc"+"\n");

            System.out.println("Wybierz opcję:");
            option = input.nextInt();
            input.nextLine();


            switch (option) {
                case ADD_PATIENT:
                    Patient patient = new Patient();
                    System.out.println("\n"+"Imię:");
                    patient.setFirstName(input.nextLine());
                    System.out.println("Nazwisko:");
                    patient.setLastName(input.nextLine());
                    System.out.println("PESEL:");
                    patient.setPesel(input.nextLine());
                    hospital.addPatient(patient);
                    break;
                case PRINT_PATIENT:
                    hospital.printPatients();
                    break;
                case EXIT:
                    System.out.println("Zamykam program.");
                    break;
                case CHECK_PATIENT:
                    hospital.checkSize();
                    break;
                default:
                    System.out.println("Nie znaleziono takiej opcji.");
            }
            
        }

        input.close();
    }

}

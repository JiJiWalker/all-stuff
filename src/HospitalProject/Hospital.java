package HospitalProject;

/**
 * Created by jakub on 19.11.16.
 */
public class Hospital {


    /*public Patient createPatient() {
        System.out.println("Imię:");
        String firstName = sc.nextLine();

        System.out.println("Nazwisko:");
        String lastName = sc.nextLine();

        System.out.println("PESEL:");
        String pesel = sc.nextLine();

        return new Patient(firstName, lastName, pesel);
    }*/

    public static final int MAX_PATIENTS_NUM = 10;
    private Patient[] patients;
    private int regPatients;

    public Hospital() {
        patients = new Patient[MAX_PATIENTS_NUM];
        regPatients = 0;
    }

    public void addPatient(Patient patient) {
        try {
            patients[regPatients] = patient;
            regPatients++;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Zapisano maksymalną liczbę pacjentów. Zapraszamy jutro."+"\n");
        }

    /*public void addPatient(Patient patient) {
        if (regPatiens < MAX_PATIENTS_NUM) {
            patients[regPatiens] = patient;
            regPatiens++;
        } else {
            System.out.println("Zapisano maxymalną liczbę pacjentów, zapraszamy jutro!");
        }
    }*/

    }

    public void printPatients() {
        for (int i=0; i<regPatients; i++) {
            System.out.println(patients[i].getFirstName()+" "+patients[i].getLastName()+" "+patients[i].getPesel()+"\n");
        }
    }

    public void checkSize() {
        System.out.println(MAX_PATIENTS_NUM-regPatients+" - ilość wolnych miejsc."+"\n");
    }

}

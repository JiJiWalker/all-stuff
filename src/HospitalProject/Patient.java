package HospitalProject;

/**
 * Created by jakub on 19.11.16.
 */
public class Patient {

    String firstName;
    String lastName;
    String pesel;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public Patient() {

    }

    public Patient(String imie, String lastName, String pesel) {
        this.setFirstName(imie);
        this.setLastName(lastName);
        this.setPesel(pesel);
    }

    public void printInfo() {
        String info = "Imię: "+ firstName +"\n"
                    +"Nazwisko: "+ lastName +"\n"
                    +"PESEL: "+pesel+"\n";
        System.out.println(info);
    }


}
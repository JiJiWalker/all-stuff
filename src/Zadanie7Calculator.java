/**
 * Created by jakub on 28.09.16.
 */
public class Zadanie7Calculator {

    double add(double a, double b){;
        return a+b;
    }

    // nie trzeba dopisywać double result. Można w return napisać wynik
    double subtract(double a, double b){
        return a-b;
    }

    double multiply(double a, double b){
        double result = a*b;
        return result;
    }

    double divide(double a, double b){
        double result = a/b;
        return result;
    }

}

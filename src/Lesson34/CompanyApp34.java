package Lesson34;


import java.util.Scanner;

/**
 * Created by Jakub on 29.03.2017.
 */
public class CompanyApp34 {

    private static final int ADD_EMPLOYEE = 0;
    private static final int FIND_EMPLOYEE = 1;
    private static final int EXIT = 2;

    private Company34 company;

    private CompanyApp34() {
        company = new Company34();
    }

    public static void main(String[] args) {
        CompanyApp34 companyApp34 = new CompanyApp34();
        try (Scanner sc = new Scanner(System.in)) {
            int userOption;
            do {
                companyApp34.printOption();
                userOption = sc.nextInt();
                sc.nextLine();

                switch (userOption) {
                    case CompanyApp34.ADD_EMPLOYEE:
                        companyApp34.addEmployee(sc);
                        break;
                    case CompanyApp34.FIND_EMPLOYEE:
                        companyApp34.findAndPrintEmployee(sc);
                        break;
                    case CompanyApp34.EXIT:
                        break;
                }
            } while (userOption != CompanyApp34.EXIT);
        }
    }

    private void printOption() {
        System.out.println("0 - Dodanie pracownika.");
        System.out.println("1 - Wyszukiwanie pracownika.");
        System.out.println("2 - Wyjście z programu.");
    }

    private void addEmployee(Scanner sc) {
        Employee34 employee34 = new Employee34();

        System.out.println("Dodawania pracownika.");
        System.out.println("Imię:");
        employee34.setFirstName(sc.nextLine());
        System.out.println("Nazwisko:");
        employee34.setLastName(sc.nextLine());
        System.out.println("Pensja:");
        employee34.setSalary(sc.nextDouble());
        sc.nextLine();

        company.addEmployee(employee34);
    }

    private void findAndPrintEmployee(Scanner sc) {
        System.out.println("Wyszukiwanie pracownika");
        System.out.println("Podaj imię:");
        String firstName = sc.nextLine();
        System.out.println("Podaj nazwisko:");
        String lastName = sc.nextLine();

        Employee34 employee34 = company.getEmployee(firstName, lastName);
        System.out.println(employee34);
    }

}

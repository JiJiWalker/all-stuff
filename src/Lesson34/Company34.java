package Lesson34;

import java.util.HashMap;

/**
 * Created by Jakub on 29.03.2017.
 */
public class Company34 {
    private HashMap<String, Employee34> employees;

    Company34() {
        employees = new HashMap<>();
    }

    boolean addEmployee(Employee34 employee34) {
        String key = employee34.getFirstName()+" "+employee34.getLastName();

        if (employees.get(key) != null) {
            return false;
        } else {
            employees.put(key, employee34);
            return true;
        }
    }

    Employee34  getEmployee(String firstName, String lastName) {
        String key = firstName+" "+lastName;
        return employees.get(key);
    }
}

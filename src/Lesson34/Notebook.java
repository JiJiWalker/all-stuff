package Lesson34;

/**
 * Created by Jakub on 29.03.2017.
 */
public class Notebook {
    private String producer;
    private String model;

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Notebook(String producer, String model) {
        this.producer = producer;
        this.model = model;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Notebook notebook = (Notebook) o;

        if (producer != null ? !producer.equals(notebook.producer) : notebook.producer != null) return false;
        return model != null ? model.equals(notebook.model) : notebook.model == null;
    }

    @Override
    public int hashCode() {
        int result = producer != null ? producer.hashCode() : 0;
        result = 31 * result + (model != null ? model.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Notebook. "+"Producer: "+producer+" Model: "+model;
    }
}

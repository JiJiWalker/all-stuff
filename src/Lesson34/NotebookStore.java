package Lesson34;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jakub on 29.03.2017.
 */
public class NotebookStore {
    public static void main(String[] args) {

        Map<String, Notebook> notebooks = new HashMap<>();
        notebooks.put("B590", new Notebook("Lenovo", "C100"));
        notebooks.put("Inspiron022", new Notebook("Dell", "Inspiron022"));
        notebooks.put("G22E400", new Notebook("HP", "G22E400"));
        notebooks.put("XPS0091V", new Notebook("Dell", "XPS0091V"));
        notebooks.put("Millenium1", new Notebook("Zoniak", "Millenium1"));
        notebooks.put("DoubleStandard", new Notebook("Sony", "DoubleStandard"));

//        Wyświetlamy zbiór kluczy
        System.out.println("Modele laptopów:");
        for (String key: notebooks.keySet()) {
            System.out.println(key);
        }

//        wyświetlamy informacje o laptopach na podstawie kluczy
        String key = "G22E400";
        String key2 = "B590";
        String key3 = "DoubleStandard"; // było kilka laptopów z tym kluczem
        System.out.println("Znaleziono laptop o kodzie G22E400: ");
        System.out.println(notebooks.get(key)+"\n");
        System.out.println("Znaleziono drugi laptop o kodzie B950");
        System.out.println(notebooks.get(key2)+"\n");
        System.out.println("Znaleziono trzeci kod DoubleStandard. Ile laptopów wyświetli?");
        System.out.println(notebooks.get(key3)+"\n"); // Wyświetliło ostatni element który jest dodany pod tym samym kluczem

//        usuwamy obiekt na podstawie klucza
        notebooks.remove("XPS0091V");
        System.out.println("Ilość produktów w sklepie: "+notebooks.size()); // oczywiście klucze są uniwersalne

        System.out.println(notebooks.entrySet());
    }
}

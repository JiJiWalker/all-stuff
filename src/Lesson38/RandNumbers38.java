package Lesson38;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Created by Jakub on 29.04.2017.
 */
public class RandNumbers38 {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        Random r = new Random();

        generateRandomNumbers(numbers, 10, () -> r.nextInt(100));
        printNumbers(numbers, p -> System.out.println(p));
        removeDevideByTwoNumber(numbers, p -> p%2 == 0);
        System.out.println(">>>");
        printNumbers(numbers, p -> System.out.println(p));
//        numbers.forEach(System.out::println); - pętla

    }

    private static <T> void generateRandomNumbers(List<T> list, int num, Supplier<T> sup) {
        for (int i=0; i<num; i++) {
            list.add(sup.get());
        }
    }

    private static <T> void printNumbers(List<T> list, Consumer<T> con) {
        for (T t: list) {
            con.accept(t);
        }
    }

    private static <T> void removeDevideByTwoNumber(List<T> list, Predicate<T> pre) {
        // wersja po skróceniu
        // list.removeIf(pre);
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            T tmp = it.next();
            if (pre.test(tmp)) {
                it.remove();
            }
        }
//        Poniższy kod nie działa
//        for (T t: list) {
//            if (pre.test(t)) {
//                list.remove(t);
//            }
//        }
    }
}

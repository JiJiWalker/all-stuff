package Lesson38;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

/**
 * Created by Jakub on 29.04.2017.
 */
public class SupplierTest {
    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<>();
        Random r = new Random();
        generate(ints, 50, () -> r.nextInt(1000));
        for (int i: ints) {
            System.out.println(i);
        }
    }

    public static <T> void generate(List<T> list, int num, Supplier<T> sup) {
        for (int i=0; i<num; i++) {
            list.add(sup.get());
        }
    }
}

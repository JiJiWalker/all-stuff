package Lesson38;

import java.util.function.Function;

/**
 * Created by jakub on 27.04.17.
 */
public class Functions {
    public static void main(String[] args) {
        // Przyjmuje String i zwraca String
        Function<String, String> func = (String str) -> str.toLowerCase().trim();
        String original = "      WIELKIE LITERY    ";
        // wywołujemy funkcję przekazując oryginal jako argument
        String lowerCaseTrim = func.apply(original);
        System.out.println(lowerCaseTrim);
    }
}

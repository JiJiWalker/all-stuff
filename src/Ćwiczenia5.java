/**
 * Created by jakub on 23.09.16.
 */


public class Ćwiczenia5 {
    public static void main(String[] args) {

        int x = 5;
        int y = 10;
        boolean logic = true;

        System.out.println("Czy X jest większy od Y?");
        System.out.println(x > y);

        System.out.println("Czy X jest mniejsze od Y?");
        System.out.println(x < y);

        System.out.println("Czy X jest różny od Y");
        System.out.println(x != y);

        boolean instance = "Kasia" instanceof String;
        System.out.println(instance);

        System.out.print("x<10 && y<11? ");
        System.out.println(x<10 && y<11);

        System.out.print("x<10 && y<10? ");
        System.out.println(x<10 && y<10);

        System.out.print("x<10 || y<10? ");
        System.out.println(x<10 || y<10);

        System.out.print(logic);
        System.out.print(", !logic = ");
        System.out.println(!logic);

        System.out.print("Warunek złożony !(x<10 && y<10) ");
        System.out.println(!(x<10 && y<10));
    }
}

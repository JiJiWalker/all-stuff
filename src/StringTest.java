/**
 * Created by jakub on 02.01.17.
 */
public class StringTest {
    public static void main(String[] args) {

        long time1;
        long startTime = System.nanoTime();
        StringBuilder builder = new StringBuilder();
        for (int i=0; i<1000; i++) {
            builder.append(i);
            builder.append(" ");
        }
        String numbers = builder.toString();
        time1 = System.nanoTime()-startTime;
        System.out.println("TimeRecorded: "+time1);
        System.out.println(numbers);

        long time2;
        startTime = System.nanoTime();
        String numbers2 = "";
        for (int i =0; i<1000; i++) {
            numbers2 = numbers2 + i + " ";
        }
        time2 = System.nanoTime()-startTime;
        System.out.println("TimeRecorded: "+time2);
        System.out.println(numbers2);

        System.out.println("TimeRecorded / TimeRecorded: "+time2/time1);
    }
}

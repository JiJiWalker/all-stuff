package Lesson37;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by jakub on 25.04.17.
 */
public class Lotto37 {
    private List<Integer> numbers;

    Lotto37() {
        numbers = new ArrayList<>();
        generate();
    }

    private void generate() {
        // generuje liczby od 1 do 49
        for (int i = 0; i <49; i++) {
            numbers.add(i);
        }
    }

    void randomize() {
        // miesza wygenerowane liczby
        Collections.shuffle(numbers);
    }

    int checkResult(List<Integer> userNumbers) {
        List<Integer> lottoResult = numbers.subList(0, 6);

        System.out.println("Wynik losowania: ");
        for (Integer num: lottoResult) {
            System.out.println(num + " ");
        }
        System.out.println(); // dodaje przerwę po wynikach, przed txt "Trafione liczby"

        int found = 0;
        for (int i=0; i<6; i++) {
            if (lottoResult.contains(userNumbers.get(i))) {
                found++;
            }
        }
        return found;
    }
}

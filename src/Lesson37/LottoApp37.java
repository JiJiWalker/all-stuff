package Lesson37;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by jakub on 25.04.17.
 */
public class LottoApp37 {
    public static void main(String[] args) {
        List<Integer> userNumbers = new ArrayList<>();

        System.out.println("Witamy w losowaniu Lotto! Podaj swoje liczby (Od 1 do 49)");
        try (Scanner sc = new Scanner(System.in)) {
            int numbers = 6;
            int i = 1;
            while (numbers > 0) {
                System.out.println("Podaj kolejną liczbę ("+i++ +")");
                userNumbers.add(sc.nextInt());
                sc.nextLine();
                numbers--;
            }
        }

        Lotto37 lotto37 = new Lotto37();
        lotto37.randomize();
        int found = lotto37.checkResult(userNumbers);
        System.out.println("Trafione liczby: "+found);
    }
}

package PolishHospitalProject;


/**
 * Created by jakub on 30.12.16.
 */
public class Hospital {

    public static final byte MAX_EMPLOYEES = 3;

    private Person[] employees;
    private int employeesNumbers;

    public Person[] getEmployees() {
        return employees;
    }

    public void setEmployees(Person[] employees) {
        this.employees = employees;
    }

    public int getEmployeesNumbers() {
        return employeesNumbers;
    }

    public void setEmployeesNumbers(int employeesNumbers) {
        this.employeesNumbers = employeesNumbers;
    }

    public Hospital() {
        setEmployees(new Person[MAX_EMPLOYEES]);
        setEmployeesNumbers(0);
    }

    public void add(Person person) {
        try {
            employees[employeesNumbers] = person;
            employeesNumbers++;
        } catch (Exception e) {
            System.out.println("Nie mamy już wolnych wakatów.");
        }
    }

    @Override
    public String toString() {
        String result = "";
        for (int i =0; i<employeesNumbers; i++){
            result = result + employees[i] +"\n";
        }
        return result;
    }
}

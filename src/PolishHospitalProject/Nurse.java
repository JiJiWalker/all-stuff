package PolishHospitalProject;

/**
 * Created by jakub on 30.12.16.
 */
public class Nurse extends Person{

    private byte overtime;

    public Nurse(String firstName, String lastName, short salary, byte overtime) {
        super(firstName, lastName, salary);
        this.overtime = overtime;
    }

    public byte getOvertime() {
        return overtime;
    }

    public void setOvertime(byte overtime) {
        this.overtime = overtime;
    }

    @Override
    public String toString() {
        return super.toString()+", Nadgodziny: "+overtime;
    }
}

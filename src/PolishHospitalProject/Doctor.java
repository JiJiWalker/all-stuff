package PolishHospitalProject;

/**
 * Created by jakub on 30.12.16.
 */
public class Doctor extends Person{

    private short bonus;

    public Doctor(String firstName, String secondName, short salary, short bonus) {
        super(firstName, secondName, salary);
        this.bonus = bonus;
    }

    public short getBonus() {
        return bonus;
    }

    public void setBonus(short bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return super.toString()+", Premia:"+bonus;
    }
}

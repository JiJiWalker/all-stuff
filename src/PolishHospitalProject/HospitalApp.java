package PolishHospitalProject;

/**
 * Created by jakub on 30.12.16.
 */
public class HospitalApp {
    public static void main(String[] args) {

        Hospital hospital = new Hospital();
        hospital.add(new Doctor("Doctor", "Strange", (short) 3000, (short) 200 ));
        hospital.add(new Nurse("Harley", "Queen", (short) 2000, (byte) 10));
        hospital.add(new Nurse("Barbie", "Girl", (short) 2000, (byte) 15));

        System.out.println("Pracownicy szpitala:"+"\n");
        System.out.println(hospital);
    }
}

package PolishHospitalProject;

/**
 * Created by jakub on 30.12.16.
 */
public class Person {

    private String firstName;
    private String lastName;
    private short salary;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public short getSalary() {
        return salary;
    }

    public void setSalary(short salary) {
        this.salary = salary;
    }

    public Person(String firstName, String lastName, short salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Imię: "+firstName+", Nazwisko: "+lastName+", Wypłata: "+salary;
    }
}
